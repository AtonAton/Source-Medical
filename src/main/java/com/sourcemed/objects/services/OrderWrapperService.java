package com.sourcemed.objects.services;

import com.sourcemed.objects.objects.OrderWrapper;

import java.util.List;

/**
 * Created by Cheeeng on 10/10/2017.
 */
public interface OrderWrapperService {
    List<OrderWrapper> findAllOrderWrapper();
    void createOrderWrapper(OrderWrapper orderWrapper);
    OrderWrapper findByOrderWrapperId(String orderWrapperId);
    List<OrderWrapper> findAllCurrentOrderWrapper(String patientId);
}
