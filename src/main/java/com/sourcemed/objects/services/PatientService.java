package com.sourcemed.objects.services;

import java.util.List;

import com.sourcemed.objects.objects.Doctor;
import com.sourcemed.objects.objects.Patient;

/**
 * Created by PC on 7/21/2017.
 */
public interface PatientService {

    //////////////////////////BASIC CRUD OPERATIONS/////////////////////////
    void addPatient(Patient patient);
    void updatePatient(Patient patient);
    void updatePatientForHandle(Patient patient, Doctor doctor);
    void deletePatient(String id);
    Patient findByPatientId(String patientId);
    List<Patient> findAllPatients();

    /////////////////////////SPECIAL FIND OPERATIONS////////////////////////
    List<Patient> findAllPatientsByLastname(String lastname);
    List<Patient> findAllPatientsByMedicalInstitution(String hospitalId);
}
