package com.sourcemed.objects.services;

import java.util.List;
import com.sourcemed.objects.objects.PhysicalExam;
/**
 * Created by PC on 8/5/2017.
 */
public interface PhysicalExamService {
    void createPhysicalExam(PhysicalExam physicalExam);
    List<PhysicalExam> findAllPhysicalExams();
    List<PhysicalExam> findAllPhysicalExamByPatientId(String patientId);
    PhysicalExam findByPhysicalExamId(String physicalExamId);
}
