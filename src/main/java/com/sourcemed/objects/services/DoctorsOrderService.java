package com.sourcemed.objects.services;

import java.util.List;
import com.sourcemed.objects.objects.DoctorsOrder;
/**
 * Created by PC on 8/5/2017.
 */
public interface DoctorsOrderService {
    void createDoctorOrder(DoctorsOrder doctorsOrder);
    List<DoctorsOrder> findAllDoctorsOrder();
    List<DoctorsOrder> findAllDoctorsOrderByPatientId(String patientId);
    DoctorsOrder findDoctorsOrderById(String orderId);
    List<DoctorsOrder> findDoctorsOrderByWrapperId(String wrapperId);
    void deleteDoctorOrder(String id);
    void updateDoctorOrder(String id, DoctorsOrder doctorsOrder);
}
