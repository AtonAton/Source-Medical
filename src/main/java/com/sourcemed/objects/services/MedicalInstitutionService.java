package com.sourcemed.objects.services;

import java.util.List;
import com.sourcemed.objects.objects.MedicalInstitution;
/**
 * Created by PC on 8/5/2017.
 */
public interface MedicalInstitutionService {

    //////////////////////////BASIC CRUD OPERATIONS/////////////////////////
    void addMedicalInstitution(MedicalInstitution medicalInstitution);
    void updateMedicalInstittution(MedicalInstitution medicalInstitution);
    void deleteMedicalInstitution(String id);
    List<MedicalInstitution> findAllMedicalInstitutions();

    /////////////////////////SPECIAL FIND OPERATIONS////////////////////////

}
