package com.sourcemed.objects.services;

import java.util.List;
import com.sourcemed.objects.objects.LaboratoryResult;
/**
 * Created by PC on 8/5/2017.
 */
public interface LaboratoryResultService {
    void createLaboratoryResult(LaboratoryResult laboratoryResult);
    List<LaboratoryResult> findAllLaboratoryResults();
    List<LaboratoryResult> findAllLaboratoryResultsByPatientId(String patientId);
}
