package com.sourcemed.objects.services;

import java.util.List;
import com.sourcemed.objects.objects.VitalSigns;
/**
 * Created by PC on 8/5/2017.
 */
public interface VitalSignsService {
    void createVitalSigns(VitalSigns vitalSigns);
    List<VitalSigns> findAllVitalSigns();
    List<VitalSigns> findAllVitalSignsByPatientId(String patientId);
    VitalSigns findByVitalSignsId(String vitalSignsId);
}
