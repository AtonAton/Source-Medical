package com.sourcemed.objects.services;

import java.util.List;

import com.sourcemed.objects.objects.Doctor;
import com.sourcemed.objects.objects.MedicalChart;
/**
 * Created by PC on 8/5/2017.
 */
public interface MedicalChartService {

    void createMedicalChart(MedicalChart medicalChart);
    List<MedicalChart> findAll();
    List<MedicalChart> findAllMedicalChartByPatientId (String patientId);
    MedicalChart findByMedicalChartId(String medRecId);
    MedicalChart findCurrentMedicalChart(List<MedicalChart> MedChartList);
    void updateMedicalChart(String medicalChartId, MedicalChart medicalChart);
    void addAttendingDoctor(String medicalChartId, Doctor doctor);
    List<MedicalChart> findAllPastMedicalCharts(String patientId);
}
