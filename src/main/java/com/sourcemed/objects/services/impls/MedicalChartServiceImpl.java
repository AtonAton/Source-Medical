package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.Doctor;
import com.sourcemed.objects.objects.MedicalChart;
import com.sourcemed.objects.repositories.MedicalChartRepository;
import com.sourcemed.objects.services.MedicalChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by PC on 8/5/2017.
 */
@Service("medicalChartService")
public class MedicalChartServiceImpl implements MedicalChartService{

    @Autowired
    MedicalChartRepository medicalChartRepository;

    @Override
    public void createMedicalChart(MedicalChart medicalChart){
        medicalChartRepository.save(medicalChart);
    }
    @Override
    public List<MedicalChart> findAllMedicalChartByPatientId(String patientId){
        List<MedicalChart> patientCharts = medicalChartRepository.findByPatientId(patientId);
        return  patientCharts;
    }
    @Override
    public List<MedicalChart> findAll(){
        return medicalChartRepository.findAll();
    }

    @Override
    public MedicalChart findByMedicalChartId(String medRecId){
        return medicalChartRepository.findByMedicalChartId(medRecId);
    }

    @Override
    public MedicalChart findCurrentMedicalChart(List<MedicalChart> MedChartList){
        MedicalChart currentMedChart = MedChartList.stream().filter(chart -> chart.isCurrent() == true).findFirst().get();
        return currentMedChart;
    }

    @Override
    public void updateMedicalChart(String medicalChartId, MedicalChart medicalChart){
        MedicalChart medchartToBeDeleted = medicalChartRepository.findByMedicalChartId(medicalChartId);
        medicalChartRepository.delete(medchartToBeDeleted.get_Id());
        medicalChartRepository.save(medicalChart);
    }

    @Override
    public void addAttendingDoctor(String medicalChartId, Doctor doctor){
        MedicalChart medChartFromDB = medicalChartRepository.findByMedicalChartId(medicalChartId);
        if(medChartFromDB.getAttendingDoctorList()== null){
            List<Doctor> attendingDoctors = new ArrayList<Doctor>();
            attendingDoctors.add(doctor);
            medChartFromDB.setAttendingDoctorList(attendingDoctors);
        }else{
            medChartFromDB.getAttendingDoctorList().add(doctor);
        }
        medicalChartRepository.save(medChartFromDB);
    }

    @Override
    public List<MedicalChart> findAllPastMedicalCharts(String patientId){
        List<MedicalChart> charts = medicalChartRepository.findByPatientId(patientId);
        List<MedicalChart> pastCharts = charts.stream().filter(chart -> !chart.isCurrent()).collect(Collectors.toList());
        return pastCharts;
    }
}
