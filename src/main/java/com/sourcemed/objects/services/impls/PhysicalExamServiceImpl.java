package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.PhysicalExam;
import com.sourcemed.objects.repositories.PhysicalExamRepository;
import com.sourcemed.objects.services.PhysicalExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
@Service("physicalExamService")
public class PhysicalExamServiceImpl implements PhysicalExamService{
    @Autowired
    PhysicalExamRepository physicalExamRepository;

    @Override
    public void createPhysicalExam(PhysicalExam physicalExam){
        physicalExamRepository.save(physicalExam);
    }
    @Override
    public List<PhysicalExam> findAllPhysicalExams(){
        return physicalExamRepository.findAll();
    }
    @Override
    public List<PhysicalExam> findAllPhysicalExamByPatientId(String patientId){
        return physicalExamRepository.findAllBypatientId(patientId);
    }
    @Override
    public PhysicalExam findByPhysicalExamId(String physicalExamId){
        return physicalExamRepository.findPhysicalExamByphysicalExamId(physicalExamId);
    }
}
