package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.OrderWrapper;
import com.sourcemed.objects.repositories.OrderWrapperRepository;
import com.sourcemed.objects.services.OrderWrapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Cheeeng on 10/10/2017.
 */

@Service("orderWrapperService")
public class OrderWrapperServiceImpl implements OrderWrapperService{
    @Autowired
    OrderWrapperRepository orderWrapperRepository;


    @Override
    public List<OrderWrapper> findAllOrderWrapper(){
        return orderWrapperRepository.findAll();
    }
    @Override
    public void createOrderWrapper(OrderWrapper orderWrapper){
        orderWrapperRepository.save(orderWrapper);
    }

    @Override
    public OrderWrapper findByOrderWrapperId(String orderWrapperId){
        return orderWrapperRepository.findOrderWrapperByorderWrapperId(orderWrapperId);
    }

    @Override
    public List<OrderWrapper> findAllCurrentOrderWrapper(String patientId){
        List<OrderWrapper> orderWrappersFromDB = orderWrapperRepository.findBypatientId(patientId);
        List<OrderWrapper> results =  orderWrappersFromDB.stream().filter(orderwrap -> orderwrap.isCurrent() == true).collect(Collectors.toList());
        return results;
    }
}
/*
List<Doctor> doctors = doctorRepository.findAll();
List<Doctor> results = doctors.stream().filter(doc -> doc.getExpertise().equals(specialty)).collect(Collectors.toList());
return results;*/
