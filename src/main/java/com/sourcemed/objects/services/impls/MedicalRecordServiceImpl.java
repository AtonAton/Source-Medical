package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.MedicalInstitution;
import com.sourcemed.objects.objects.MedicalRecord;
import com.sourcemed.objects.repositories.MedicalRecordRepository;
import com.sourcemed.objects.services.MedicalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("medicalRecordService")
public class MedicalRecordServiceImpl implements MedicalRecordService{
    @Autowired
    MedicalRecordRepository medicalRecordRepository;


    @Override
    public List<MedicalRecord> findAll(){
        return medicalRecordRepository.findAll();
    }

    @Override
    public void createMedicalRecord(MedicalRecord medicalRecord){
        medicalRecordRepository.save(medicalRecord);
    }

    @Override
    public MedicalRecord findMedRecordByPatientId(String patientId){
        return medicalRecordRepository.findMedRecordByPatientId(patientId);
    }

    @Override
    public MedicalRecord findMedRecordByMedRecId(String medRecId){
        return medicalRecordRepository.findByMedicalRecordId(medRecId);
    }
}
