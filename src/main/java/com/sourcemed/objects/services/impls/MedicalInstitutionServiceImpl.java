package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.MedicalInstitution;
import com.sourcemed.objects.repositories.MedicalInstitutionRepository;
import com.sourcemed.objects.services.MedicalInstitutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
@Service("medicalInstitutionService")
public class MedicalInstitutionServiceImpl implements MedicalInstitutionService{

    SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    @Autowired
    MedicalInstitutionRepository medicalInstitutionRepository;

    //////////////////////////////////////////BASIC CRUD OPERATIONS////////////////////////////////////////////////////
    @Override
    public List<MedicalInstitution> findAllMedicalInstitutions(){
        return medicalInstitutionRepository.findAll();
    }

    @Override
    public void addMedicalInstitution(MedicalInstitution medicalInstitution){
        medicalInstitution.setCreated_at(date.format(new Date()));
        medicalInstitution.setIsActive(true);
        medicalInstitutionRepository.save(medicalInstitution);
    }
    @Override
    public void updateMedicalInstittution(MedicalInstitution medicalInstitution){
        medicalInstitution.setUpdated_at(date.format(new Date()));
        medicalInstitutionRepository.save(medicalInstitution);
    }
    @Override
    public void deleteMedicalInstitution(String id){
        MedicalInstitution medInstiToBeDeleted = medicalInstitutionRepository.findById(id);
        medInstiToBeDeleted.setIsActive(false);
    }

    /////////////////////////////////////////SPECIFIC FIND OPERATIONS///////////////////////////////////////////////////


}
