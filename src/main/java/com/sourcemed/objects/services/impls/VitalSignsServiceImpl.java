package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.VitalSigns;
import com.sourcemed.objects.repositories.VitalSignsRepository;
import com.sourcemed.objects.services.VitalSignsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
@Service("vitalSignsService")
public class VitalSignsServiceImpl implements VitalSignsService{
    @Autowired
    VitalSignsRepository vitalSignsRepository;

    @Override
    public void createVitalSigns(VitalSigns vitalSigns){
        vitalSignsRepository.save(vitalSigns);
    }

    @Override
    public List<VitalSigns> findAllVitalSigns(){
        return vitalSignsRepository.findAll();
    }

    @Override
    public List<VitalSigns> findAllVitalSignsByPatientId(String patientId){
        return vitalSignsRepository.findVitalSignsBypatientId(patientId);
    }

    @Override
    public VitalSigns findByVitalSignsId(String vitalSignsId){
        return vitalSignsRepository.findByvitalSignsId(vitalSignsId);
    }
}
