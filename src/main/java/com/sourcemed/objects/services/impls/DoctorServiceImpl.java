package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.*;
import com.sourcemed.objects.repositories.*;
import com.sourcemed.objects.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by PC on 8/5/2017.
 */
@Service("doctorService")
public class DoctorServiceImpl implements DoctorService {

    SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    @Autowired
    DoctorRepository doctorRepository;
    MedicalStaffRepository medicalStaffRepository;
    MedicalRecordRepository medicalRecordRepository;
    MedicalInstitutionRepository medicalInstitutionRepository;

    @Autowired
    PatientRepository patientRepository;

    //////////////////////////////////////////BASIC CRUD OPERATIONS////////////////////////////////////////////////////
    @Override
    public List<Doctor> findAllDoctors(){
        return doctorRepository.findAll();
    }

    @Override
    public void addDoctor(Doctor doctor){
        doctor.setCreated_at(date.format(new Date()));
        doctor.setIsActive(true);
        doctorRepository.save(doctor);
    }

    @Override
    public void updateDoctor(Doctor doctor){
        doctor.setUpdated_at(date.format(new Date()));
        doctorRepository.save(doctor);
    }

    @Override
    public void deleteDoctor(String id){
        Doctor doctorToBeDeleted = doctorRepository.findById(id);
        doctorToBeDeleted.setUpdated_at(date.format(new Date()));
        doctorToBeDeleted.setIsActive(false);
    }

    /////////////////////////////////////////SPECIFIC FIND OPERATIONS///////////////////////////////////////////////////
    @Override
    public Doctor findDoctorById(String id){
        List<Doctor> doctors = doctorRepository.findAll();
        Doctor result = doctors.stream().filter(doc -> doc.getId().equals(id)).findFirst().get();
        return result;
    }
    @Override
    public Doctor findDoctorBydoctorId(String id){
        return doctorRepository.findBydoctorId(id);
    }

    @Override
    public List<Doctor> findAllDoctorsByLastname(String lastname){
        List<Doctor> doctors = doctorRepository.findAll();
        List<Doctor> results = doctors.stream().filter(doc -> doc.getLastName().equals(lastname)).collect(Collectors.toList());
        return results;
    }

    @Override
    public List<Doctor> findAllDoctorsBySpecialty(String specialty){
        List<Doctor> doctors = doctorRepository.findAll();
        List<Doctor> results = doctors.stream().filter(doc -> doc.getExpertise().equals(specialty)).collect(Collectors.toList());
        return results;
    }

    @Override
    public List<Doctor> findAllDoctorsByMedicalInstitution(String medInsti_id){
        MedicalInstitution hospital = medicalInstitutionRepository.findById(medInsti_id);
        List<Doctor> doctors = doctorRepository.findAll();
        List<Doctor> results = doctors.stream().filter(doc -> doc.getMedicalInstitution().contains(hospital)).collect(Collectors.toList());
        return results;
    }

    @Override
     public List<Patient> findAllPatientsHandled(Doctor doctor){
        if(doctor.getPatientsHandled()!= null){
            List<Patient> patientsHandled = doctor.getPatientsHandled();
            return patientsHandled;
        }else{
            return null;
        }

    }

    @Override
    public List<Patient> findAllPatientsAttended(Doctor doctor){
        List<Patient> patientsAttended = doctor.getPatientsAttended();
        return patientsAttended;
    }

    @Override
    public void addToHandledPatient(Doctor doctor, Patient patient){
        String patientId = patient.getPatientId();
        Patient patientFromPatientRepo = patientRepository.findBypatientId(patientId);
        if(doctor.getPatientsHandled()==null){
            List<Patient> handledPatients = new ArrayList<Patient>();
            patient.setIsHandled(true);
            patientFromPatientRepo.setIsHandled(true);
            handledPatients.add(patient);
            doctor.setPatientsHandled(handledPatients);
            doctorRepository.save(doctor);
        }
        else{
            patient.setIsHandled(true);
            patientFromPatientRepo.setIsHandled(true);
            doctor.getPatientsHandled().add(patient);
            doctorRepository.save(doctor);
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public MedicalInstitution findById(String id){
        return medicalInstitutionRepository.findById(id);
    }

    @Override
    public Doctor loginVerification(String userId, String pass){
        Doctor doctor = doctorRepository.findByusername(userId);
        if(doctor !=null && doctor.getPassword().equals(pass)){
            return doctor;
        }
        return null;
    }

    @Override
    public Boolean duplicateHandlePatient(Patient patient, Doctor doctor){
        List<Patient> currentlyHandledPatients = doctor.getPatientsHandled();
        String patientIdOfPatientToBeAdded = patient.getPatientId();
        for(int x = 0; x<currentlyHandledPatients.size();x++){
            if(currentlyHandledPatients.get(x).getPatientId().equals(patientIdOfPatientToBeAdded)){
                return true;
            }
        }
        return false;
    }

    @Override
    public void updateHandledPatient(Patient patient, Doctor doctor){
        List<Patient> patientsHandled = doctor.getPatientsHandled();
        String patientIdOfPatientToBeUpdated = patient.getPatientId();
        for(int x = 0; x<patientsHandled.size();x++){
            if(patientsHandled.get(x).getPatientId().equals(patientIdOfPatientToBeUpdated)){
                patientsHandled.remove(x);
                patientsHandled.add(patient);
            }
        }
        doctor.setPatientsHandled(patientsHandled);
        doctorRepository.save(doctor);
    }

    @Override
    public void addPatientToAttended(Patient patient, String doctorId){
        Doctor doctorFromDB = doctorRepository.findById(doctorId);
        if(doctorFromDB.getPatientsAttended() == null){
            List<Patient> listOfPatientsAttended = new ArrayList<Patient>();
            listOfPatientsAttended.add(patient);
            doctorFromDB.setPatientsAttended(listOfPatientsAttended);
        }else{
            doctorFromDB.getPatientsAttended().add(patient);
        }
        doctorRepository.save(doctorFromDB);
    }


}
