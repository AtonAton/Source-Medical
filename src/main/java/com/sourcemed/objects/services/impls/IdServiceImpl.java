package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.ID;
import com.sourcemed.objects.services.IdService;
import com.sourcemed.objects.repositories.IdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
/**
 * Created by PC on 8/5/2017.
 */
@Service("idService")
public class IdServiceImpl implements IdService {
    @Autowired
    IdRepository idRepository;


    @Override
    public List<ID> findIdsByPatientId(String patient_id){
        List<ID> ids = idRepository.findIdsBypatientId(patient_id);
        return ids;
    }

    @Override
    public List<ID> findAllIds(){
        return idRepository.findAll();
    }
    @Override
    public void createId(ID id){idRepository.save(id);}
}
