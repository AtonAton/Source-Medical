package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.DoctorsOrder;
import com.sourcemed.objects.repositories.DoctorsOrderRepository;
import com.sourcemed.objects.services.DoctorsOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
@Service("doctorsOrderService")
public class DoctorsOrderServiceImpl implements DoctorsOrderService{
    @Autowired
    DoctorsOrderRepository doctorsOrderRepository;

    @Override
    public void createDoctorOrder(DoctorsOrder doctorsOrder){
        doctorsOrderRepository.save(doctorsOrder);
    }

    @Override
    public List<DoctorsOrder> findAllDoctorsOrder(){
       return doctorsOrderRepository.findAll();
    }

    @Override
    public List<DoctorsOrder> findAllDoctorsOrderByPatientId(String patientId){
        return doctorsOrderRepository.findAllDoctorsOrdersBypatientId(patientId);
    }
    @Override
    public DoctorsOrder findDoctorsOrderById(String orderId){
        return doctorsOrderRepository.findDoctorsOrderByorderId(orderId);
    }
    @Override
    public List<DoctorsOrder> findDoctorsOrderByWrapperId(String wrapperId){
        return doctorsOrderRepository.findByorderWrapperId(wrapperId);
    }

    @Override
    public void deleteDoctorOrder(String id){
        DoctorsOrder order = doctorsOrderRepository.findDoctorsOrderByorderId(id);
        doctorsOrderRepository.delete(order.get_Id());
    }

    @Override
    public void updateDoctorOrder(String id, DoctorsOrder doctorsOrder){
        DoctorsOrder orderToBeDeleted = doctorsOrderRepository.findDoctorsOrderByorderId(id);
        doctorsOrderRepository.delete(orderToBeDeleted.get_Id());
        doctorsOrderRepository.save(doctorsOrder);
    }
}
