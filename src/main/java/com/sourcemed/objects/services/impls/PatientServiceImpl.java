package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.Doctor;
import com.sourcemed.objects.objects.Patient;
import com.sourcemed.objects.repositories.PatientRepository;
import com.sourcemed.objects.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by PC on 7/21/2017.
 */
@Service("patientService")
public class PatientServiceImpl implements PatientService {

    SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    @Autowired
    PatientRepository patientRepository;

    //////////////////////////////////////////BASIC CRUD OPERATIONS////////////////////////////////////////////////////
    ////////////////////////////////////////////////PATIENTS///////////////////////////////////////////////////////////
    @Override
    public List<Patient> findAllPatients(){
        return patientRepository.findAll();
    }
    @Override
    public void addPatient(Patient patient){
        patient.setIsActive(true);
        patient.setCreated_at(date.format(new Date()));
        patientRepository.save(patient);
    }
    @Override
     public void updatePatient(Patient patient){
        patient.setUpdated_at(date.format(new Date()));
        patientRepository.save(patient);
    }
    @Override
    public void updatePatientForHandle(Patient patient, Doctor doctor){
        patient.setHandleBy(doctor.getFirstName()+" "+doctor.getMiddleName()+" "+doctor.getLastName()+" M.D.");
        patient.setUpdated_at(date.format(new Date()));
        patient.setIsHandled(true);
        patientRepository.save(patient);
    }

    @Override
    public void deletePatient(String id){
        Patient patientToBeDeleted = patientRepository.findById(id);
        patientToBeDeleted.setUpdated_at(date.format(new Date()));
        patientToBeDeleted.setIsActive(false);
    }
    /////////////////////////////////////////SPECIFIC FIND OPERATIONS///////////////////////////////////////////////////
    ////////////////////////////////////////////////PATIENTS////////////////////////////////////////////////////////////

    @Override
    public Patient findByPatientId(String patientId){
        return patientRepository.findBypatientId(patientId);
    }

    @Override
    public List<Patient> findAllPatientsByLastname(String lastname){
        List<Patient> patientsFromDb = patientRepository.findAll();
        List<Patient> results = patientsFromDb.stream().filter(patient -> patient.getLastName().equals(lastname)).collect(Collectors.toList());
        return results;
    }

    @Override
    public List<Patient> findAllPatientsByMedicalInstitution(String hospitalId){
        List<Patient> patientsFromDb = patientRepository.findAll();
        List<Patient> results = patientsFromDb.stream().filter(patient -> patient.getCurrentHospital_Id().equals(hospitalId)).collect(Collectors.toList());
        return results;
    }

}
