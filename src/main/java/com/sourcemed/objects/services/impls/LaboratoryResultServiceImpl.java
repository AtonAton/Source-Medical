package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.LaboratoryResult;
import com.sourcemed.objects.repositories.LaboratoryResultRepository;
import com.sourcemed.objects.services.LaboratoryResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
@Service("laboratoryService")
public class LaboratoryResultServiceImpl implements LaboratoryResultService{
    @Autowired
    LaboratoryResultRepository laboratoryResultRepository;

    @Override
    public void createLaboratoryResult(LaboratoryResult laboratoryResult){
        laboratoryResultRepository.save(laboratoryResult);
    }

    @Override
    public List<LaboratoryResult> findAllLaboratoryResults(){
        return laboratoryResultRepository.findAll();
    }

    @Override
    public List<LaboratoryResult> findAllLaboratoryResultsByPatientId(String patientId){
        return laboratoryResultRepository.findIdsBypatientId(patientId);
    }
}
