package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.MedicalStaff;
import com.sourcemed.objects.repositories.MedicalStaffRepository;
import com.sourcemed.objects.services.MedicalStaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ATON on 8/5/2017.
 */
@Service("medicalStaffService")
public class MedicalStaffServiceImpl implements MedicalStaffService{
    @Autowired
    MedicalStaffRepository medicalStaffRepository;

    @Override
    public void createMedicalStaff(MedicalStaff medicalStaff){
        medicalStaffRepository.save(medicalStaff);
    }

    @Override
    public MedicalStaff findByMedicalStaffId(String medicalStaffId){
        return medicalStaffRepository.findBymedicalStaffId(medicalStaffId);
    }

    @Override
    public List<MedicalStaff> findAllMedicalStaff(){
        return medicalStaffRepository.findAll();
    }
}
