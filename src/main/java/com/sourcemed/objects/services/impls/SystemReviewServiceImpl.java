package com.sourcemed.objects.services.impls;

import com.sourcemed.objects.objects.SystemReview;
import com.sourcemed.objects.repositories.SystemReviewRepository;
import com.sourcemed.objects.services.SystemReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
@Service("systemReviewService")
public class SystemReviewServiceImpl implements SystemReviewService{
    @Autowired
    SystemReviewRepository systemReviewRepository;

    @Override
    public List<SystemReview> findAllSystemReviews(){
        return systemReviewRepository.findAll();
    }

    @Override
    public void createSytemReview(SystemReview systemReview){
        systemReviewRepository.save(systemReview);
    }

    @Override
    public List<SystemReview> findSystemReviewsByPatientId(String patientId){
        return systemReviewRepository.findBypatientId(patientId);
    }

    @Override
    public SystemReview findBySystemReviewId(String systemReviewId){
        return systemReviewRepository.findBysystemReviewId(systemReviewId);
    }
}
