package com.sourcemed.objects.services;

import java.util.List;
import com.sourcemed.objects.objects.SystemReview;
/**
 * Created by PC on 8/5/2017.
 */
public interface SystemReviewService {
    void createSytemReview(SystemReview systemReview);
    List<SystemReview> findAllSystemReviews();
    List<SystemReview> findSystemReviewsByPatientId(String patientId);
    SystemReview findBySystemReviewId(String systemReviewId);

}
