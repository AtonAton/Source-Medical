package com.sourcemed.objects.services;

import java.util.List;
import com.sourcemed.objects.objects.MedicalRecord;
/**
 * Created by PC on 8/5/2017.
 */
public interface MedicalRecordService {
    void createMedicalRecord(MedicalRecord medicalRecord);
    List<MedicalRecord> findAll();
    MedicalRecord findMedRecordByPatientId(String patientId);
    MedicalRecord findMedRecordByMedRecId(String medRecId);

}
