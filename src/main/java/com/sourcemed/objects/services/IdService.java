package com.sourcemed.objects.services;

import java.util.List;
import com.sourcemed.objects.objects.ID;
/**
 * Created by PC on 8/5/2017.
 */
public interface IdService {

    List<ID> findIdsByPatientId(String patient_id);
    void createId(ID id);
    List<ID> findAllIds();
}
