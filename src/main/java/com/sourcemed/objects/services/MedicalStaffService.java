package com.sourcemed.objects.services;

import java.util.List;
import com.sourcemed.objects.objects.MedicalStaff;
/**
 * Created by PC on 8/5/2017.
 */
public interface MedicalStaffService {
    void createMedicalStaff(MedicalStaff medicalStaff);
    MedicalStaff findByMedicalStaffId(String medicalStaffId);
    List<MedicalStaff> findAllMedicalStaff();
}
