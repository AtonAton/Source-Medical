package com.sourcemed.objects.services;

import java.util.List;

import com.sourcemed.objects.objects.*;

/**
 * Created by PC on 8/5/2017.
 */
public interface DoctorService {

    //////////////////////////BASIC CRUD OPERATIONS/////////////////////////
    void addDoctor(Doctor doctor);
    void updateDoctor(Doctor doctor);
    void deleteDoctor(String id);
    Doctor findDoctorById(String id);
    Doctor findDoctorBydoctorId(String id);
    List<Doctor> findAllDoctors();
    void addToHandledPatient(Doctor doctor,Patient patient);
    Boolean duplicateHandlePatient(Patient patient, Doctor doctor);

    /////////////////////////SPECIAL FIND OPERATIONS////////////////////////
    List<Doctor> findAllDoctorsByLastname(String lastname);
    List<Doctor> findAllDoctorsBySpecialty(String specialty);
    List<Doctor> findAllDoctorsByMedicalInstitution(String id);
    Doctor loginVerification(String userId, String pass);

    MedicalInstitution findById(String id);
    List<Patient> findAllPatientsHandled(Doctor doctor);
    List<Patient> findAllPatientsAttended(Doctor doctor);
    void updateHandledPatient(Patient patient, Doctor doctor);
    void addPatientToAttended(Patient patient, String doctorId);

}
