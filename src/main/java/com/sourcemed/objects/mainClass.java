package com.sourcemed.objects;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by PC on 7/23/2017.
 */
@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
@EnableMongoRepositories

public class mainClass {
    public static void main(String args[]) {

        SpringApplication.run(mainClass.class, args);
    }
}
