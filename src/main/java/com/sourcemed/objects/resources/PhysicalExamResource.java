package com.sourcemed.objects.resources;

import com.sourcemed.objects.objects.PhysicalExam;
import com.sourcemed.objects.services.PhysicalExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
@Controller
@RequestMapping("/physicalExams")
public class PhysicalExamResource {
    @Autowired
    PhysicalExamService physicalExamService;

    @ResponseBody
    @RequestMapping(value="/findAllPhysicalExams", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<PhysicalExam> findAll(){
        return physicalExamService.findAllPhysicalExams();
    }

    @RequestMapping(value="/create", method = RequestMethod.POST)
    protected ResponseEntity<Void> createSystemReview(@RequestBody PhysicalExam physicalExam){
        physicalExamService.createPhysicalExam(physicalExam);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ResponseBody
    @RequestMapping(value="/findAllPhysicalExamByPatientId/{patientId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<PhysicalExam> findByPatientId(@PathVariable String patientId){
        return physicalExamService.findAllPhysicalExamByPatientId(patientId);
    }

    @RequestMapping(value="/checkIfIdExists/{id}", method=RequestMethod.POST)
    protected ResponseEntity<Void> idChecker(@PathVariable String id){
        PhysicalExam exam = physicalExamService.findByPhysicalExamId(id);
        if(exam == null){
            return new ResponseEntity<>(HttpStatus.FOUND);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
