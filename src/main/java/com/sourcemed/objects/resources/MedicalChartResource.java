package com.sourcemed.objects.resources;

import com.sourcemed.objects.objects.Doctor;
import com.sourcemed.objects.objects.MedicalChart;
import com.sourcemed.objects.services.MedicalChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Created by PC on 8/5/2017.
 */
@Controller
@RequestMapping("/medicalCharts")
public class MedicalChartResource {
    @Autowired
    MedicalChartService medicalChartService;

    @ResponseBody
    @RequestMapping(value = "/findAllMedCharts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<MedicalChart> findAllCharts(){
        List<MedicalChart> charts = medicalChartService.findAll();
        return charts;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    protected ResponseEntity<Void> createMedicalChart(@RequestBody MedicalChart medicalChart){
        medicalChartService.createMedicalChart(medicalChart);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{medicalChartId}", method = RequestMethod.POST)
    protected ResponseEntity<Void> updateMedicalChart(@PathVariable String medicalChartId, @RequestBody MedicalChart medicalChart){
        medicalChartService.updateMedicalChart(medicalChartId,medicalChart);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @ResponseBody
    @RequestMapping(value = "/findAllMedChartsByPatientId/{patientID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<MedicalChart> findAllChartsByPatientId(@PathVariable String patientID){
        List<MedicalChart> charts = medicalChartService.findAllMedicalChartByPatientId(patientID);
        return charts;
    }

    @ResponseBody
    @RequestMapping(value = "/findCurrentMedChartByPatientId/{patientID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected MedicalChart findCurrentMedChartByPatientId(@PathVariable String patientID){
        List<MedicalChart> charts = medicalChartService.findAllMedicalChartByPatientId(patientID);
        MedicalChart currentMedChart = medicalChartService.findCurrentMedicalChart(charts);
        return currentMedChart;
    }

    @ResponseBody
    @RequestMapping(value = "/findPastMedChartByPatientId/{patientID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<MedicalChart> findPastMedChartByPatientId(@PathVariable String patientID){
        return medicalChartService.findAllPastMedicalCharts(patientID);
    }

    @RequestMapping(value="/addAttendingDoctor/{id}", method=RequestMethod.POST)
    protected ResponseEntity<Void> addAttendingDoctor(@PathVariable String id, @RequestBody Doctor doctor){
        medicalChartService.addAttendingDoctor(id,doctor);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @RequestMapping(value="/checkIfIdExists/{id}", method=RequestMethod.POST)
    protected ResponseEntity<Void> idChecker(@PathVariable String id){
        MedicalChart medicalChart = medicalChartService.findByMedicalChartId(id);
        if(medicalChart == null){
            return new ResponseEntity<>(HttpStatus.FOUND);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

}
