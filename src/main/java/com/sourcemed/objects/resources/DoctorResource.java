package com.sourcemed.objects.resources;

import com.sourcemed.objects.objects.Doctor;
import com.sourcemed.objects.objects.MedicalInstitution;
import com.sourcemed.objects.objects.Patient;
import com.sourcemed.objects.services.DoctorService;
import com.sourcemed.objects.services.MedicalInstitutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */

@Controller
@RequestMapping("/doctors")
public class DoctorResource {

    @Autowired
    DoctorService doctorService;
    MedicalInstitutionService medicalInstitutionService;

    //////////////////////////////////////////BASIC CRUD OPERATIONS/////////////////////////////////////////////////////
    @ResponseBody
    @RequestMapping(value="/findAllDoctors", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<Doctor> findAllDoctors(){
        List<Doctor> doctors = doctorService.findAllDoctors();
        return doctors;
    }

    @RequestMapping(value="/createDoctor", method=RequestMethod.POST)
     protected ResponseEntity<Void> createDoctor(@RequestBody Doctor doctor){
        doctorService.addDoctor(doctor);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value="/updateDoctor", method=RequestMethod.POST)
    protected ResponseEntity<Void> updateDoctor(@RequestBody Doctor doctor){
        doctorService.updateDoctor(doctor);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }


    @RequestMapping(value="/deleteDoctor/{id}", method=RequestMethod.POST)
    protected ResponseEntity<Void> deleteDoctor(@PathVariable String id){
        Doctor doctorToBeDeleted = doctorService.findDoctorById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    /////////////////////////////////////////SPECIFIC FIND OPERATIONS///////////////////////////////////////////////////
    @ResponseBody
    @RequestMapping(value="/findAllDoctorsBySpecialty/{specialty}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<Doctor> findAllDoctorsBySpecialty(@PathVariable String specialty){
        List<Doctor> results = doctorService.findAllDoctorsBySpecialty(specialty);
        return results;
    }

    @ResponseBody
    @RequestMapping(value="/findAllDoctorsByLastName/{lastname}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<Doctor> findAllDoctorsByLastname(@PathVariable String lastname){
        List<Doctor> results = doctorService.findAllDoctorsByLastname(lastname);
        return results;
    }

    @ResponseBody
    @RequestMapping(value="/findAllDoctorsByMedicalInstitution/{hospital_id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<Doctor> findAllDoctorsByMedicalInstitution(@PathVariable String hospital_id){
        List<Doctor> results = doctorService.findAllDoctorsByMedicalInstitution(hospital_id);
        return results;
    }

    @ResponseBody
    @RequestMapping(value="/findDoctorById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected Doctor findDoctorById(@PathVariable String id){
        Doctor results = doctorService.findDoctorById(id);
        return results;
    }


    @ResponseBody
    @RequestMapping(value="/findDoctorByUserId/{username}/{pass}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected Doctor findByUserId(@PathVariable String username, @PathVariable String pass){
        Doctor result = doctorService.loginVerification(username, pass);
        return result;
    }
    @ResponseBody
    @RequestMapping(value="/patientsHandled/{username}/{pass}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<Patient> findPatientsHandled(@PathVariable String username, @PathVariable String pass){
        Doctor doctor = doctorService.loginVerification(username, pass);
        List<Patient> result = doctorService.findAllPatientsHandled(doctor);
        return result;
    }



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @ResponseBody
    @RequestMapping(value="/findAllMedicalInstitutions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<MedicalInstitution> findAllMedicalInstitutions(){
        List<MedicalInstitution> medInstitutions = medicalInstitutionService.findAllMedicalInstitutions();
        return medInstitutions;
    }
    @RequestMapping(value="/addToHandledPatients/{username}/{pass}", method=RequestMethod.POST)
    protected ResponseEntity<Void> addToHandledPatients(@PathVariable String username,@PathVariable String pass, @RequestBody Patient patient){
        Doctor doctorFromDB = doctorService.loginVerification(username,pass);
        if(doctorFromDB.getPatientsHandled()!= null){
            Boolean isDuplicate = doctorService.duplicateHandlePatient(patient,doctorFromDB);
            if(!isDuplicate){
                doctorService.addToHandledPatient(doctorFromDB, patient);
                return new ResponseEntity<>(HttpStatus.CREATED);
            }
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
        doctorService.addToHandledPatient(doctorFromDB, patient);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    @RequestMapping(value="/updateHandledPatients/{username}/{pass}", method=RequestMethod.POST)
    protected ResponseEntity<Void> updateHandledPatient(@PathVariable String username,@PathVariable String pass, @RequestBody Patient patient){
        Doctor docFromDb = doctorService.loginVerification(username,pass);
        doctorService.updateHandledPatient(patient,docFromDb);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @RequestMapping(value="/addToPatientsAttended/{id}", method=RequestMethod.POST)
    protected ResponseEntity<Void> updateHandledPatient(@PathVariable String id, @RequestBody Patient patient){
        doctorService.addPatientToAttended(patient,id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @ResponseBody
    @RequestMapping(value="/findAllPatientsAttended/{username}/{pass}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<Patient> findAllPatientsAttended(@PathVariable String username,@PathVariable String pass){
        Doctor doctorFromDb = doctorService.loginVerification(username,pass);
        return doctorService.findAllPatientsAttended(doctorFromDb);
    }



}

