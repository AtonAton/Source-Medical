package com.sourcemed.objects.resources;

import com.sourcemed.objects.objects.DoctorsOrder;
import com.sourcemed.objects.services.DoctorsOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */

@Controller
@RequestMapping("/doctorsOrder")
public class DoctorsOrderResource {
    @Autowired
    DoctorsOrderService doctorsOrderService;

    @ResponseBody
    @RequestMapping(value="/findAllDoctorsOrder", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<DoctorsOrder> findAllDoctors(){
        List<DoctorsOrder> orders = doctorsOrderService.findAllDoctorsOrder();
        return orders;
    }

    @RequestMapping(value="/createDoctorsOrder", method=RequestMethod.POST)
    protected ResponseEntity<Void> createDoctor(@RequestBody DoctorsOrder doctorsOrder){
        doctorsOrderService.createDoctorOrder(doctorsOrder);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value="/checkIfIdExists/{id}", method=RequestMethod.POST)
    protected ResponseEntity<Void> idChecker(@PathVariable String id){
        DoctorsOrder order = doctorsOrderService.findDoctorsOrderById(id);
        if(order == null){
            return new ResponseEntity<>(HttpStatus.FOUND);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @ResponseBody
    @RequestMapping(value="/findAllOrdersByWrapperId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<DoctorsOrder> findAllDoctors(@PathVariable String id){
        List<DoctorsOrder> orders = doctorsOrderService.findDoctorsOrderByWrapperId(id);
        return orders;
    }

    @RequestMapping(value="/delete/{orderId}", method=RequestMethod.POST)
    protected ResponseEntity<Void> deleteOrder(@PathVariable String orderId){
        System.out.println(orderId + " - from Resource");
        doctorsOrderService.deleteDoctorOrder(orderId);
        return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
    }

    @ResponseBody
    @RequestMapping(value="/findByOrderId/{orderId}", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected DoctorsOrder findByOrderId(@PathVariable String orderId){
        return doctorsOrderService.findDoctorsOrderById(orderId);
    }

    @RequestMapping(value="/update/{orderId}", method=RequestMethod.POST)
    protected ResponseEntity<Void> updateOrder(@PathVariable String orderId,@RequestBody DoctorsOrder doctorsOrder){
        doctorsOrderService.updateDoctorOrder(orderId,doctorsOrder);
        return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
    }


}
