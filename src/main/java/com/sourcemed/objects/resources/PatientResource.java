package com.sourcemed.objects.resources;

import com.sourcemed.objects.objects.Doctor;
import com.sourcemed.objects.objects.Patient;
import com.sourcemed.objects.services.DoctorService;
import com.sourcemed.objects.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by Aton on 7/21/2017.
 */

@Controller
@RequestMapping("/patients")
public class PatientResource {

    @Autowired
    PatientService patientService;
    @Autowired
    DoctorService doctorService;

    @RequestMapping(method = RequestMethod.GET)
    public String getPatientsPage() {
        return "users";
    }

    //////////////////////////////////////////BASIC CRUD OPERATIONS/////////////////////////////////////////////////////
    @ResponseBody
    @RequestMapping(value="/findAllPatients", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<Patient> findAllPatients(){
        List<Patient> patients = patientService.findAllPatients();
        return patients;

    }
    @ResponseBody
    @RequestMapping(value="/findPatientById/{patientId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected Patient findBypatientId(@PathVariable String patientId){
        return patientService.findByPatientId(patientId);
    }

    @RequestMapping(value="/createPatient", method=RequestMethod.POST)
    protected ResponseEntity<Void> createPatient(@RequestBody Patient patient){
        patientService.addPatient(patient);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value="/updatePatient", method=RequestMethod.POST)
    protected ResponseEntity<Void> updatePatient(@RequestBody Patient patient){
        patientService.updatePatient(patient);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
    @RequestMapping(value="/updatePatientForHandle/{docUser}/{docPass}", method=RequestMethod.POST)
    protected ResponseEntity<Void> updatePatientForHandle(@RequestBody Patient patient,@PathVariable String docUser, @PathVariable String docPass){
        Doctor doctorFromDb = doctorService.loginVerification(docUser,docPass);
        System.out.print(doctorFromDb);
        patientService.updatePatientForHandle(patient,doctorFromDb);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @RequestMapping(value="/deletePatient/{id}", method=RequestMethod.POST)
    protected ResponseEntity<Void> deletePatient(@PathVariable String id){
        patientService.deletePatient(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }


    /////////////////////////////////////////SPECIFIC FIND OPERATIONS///////////////////////////////////////////////////
    @ResponseBody
    @RequestMapping(value="/findAllPatientsByLastname/{lastname}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<Patient> findAllPatientsByLastname(@PathVariable String lastname){
        List<Patient> patients = patientService.findAllPatientsByLastname(lastname);
        return patients;

    }

    @ResponseBody
    @RequestMapping(value="/findAllPatientsByMedicalInstitution/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<Patient> findAllPatientsByMedicalInstitution(@PathVariable String id){
        List<Patient> patients = patientService.findAllPatientsByMedicalInstitution(id);
        return patients;

    }
    @RequestMapping(value="/checkIfIdExists/{id}", method=RequestMethod.POST)
    protected ResponseEntity<Void> idChecker(@PathVariable String id){
        Patient patient = patientService.findByPatientId(id);
        if(patient == null){
            return new ResponseEntity<>(HttpStatus.FOUND);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
