package com.sourcemed.objects.resources;

import com.sourcemed.objects.services.MedicalInstitutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * Created by PC on 8/5/2017.
 */
@Controller
@RequestMapping("/medicalInstitutions")
public class MedicalInstitutionResource {
    @Autowired
    MedicalInstitutionService medicalInstitutionService;
}
