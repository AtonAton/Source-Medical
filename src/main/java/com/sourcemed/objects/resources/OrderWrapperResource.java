package com.sourcemed.objects.resources;

import com.sourcemed.objects.objects.OrderWrapper;
import com.sourcemed.objects.services.OrderWrapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Cheeeng on 10/10/2017.
 */
@Controller
@RequestMapping("/orderWrappers")
public class OrderWrapperResource {

    @Autowired
    OrderWrapperService orderWrapperService;

    @RequestMapping(value="/create", method = RequestMethod.POST)
    protected ResponseEntity<Void> createSystemReview(@RequestBody OrderWrapper orderWrapper){
        orderWrapperService.createOrderWrapper(orderWrapper);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value="/checkIfIdExists/{id}", method= RequestMethod.POST)
    protected ResponseEntity<Void> idChecker(@PathVariable String id){
        OrderWrapper order = orderWrapperService.findByOrderWrapperId(id);
        if(order == null){
            return new ResponseEntity<>(HttpStatus.FOUND);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    @ResponseBody
    @RequestMapping(value="/findAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<OrderWrapper> findAll(){
        return orderWrapperService.findAllOrderWrapper();
    }

    @ResponseBody
    @RequestMapping(value="/findAllCurrentOrderWrapper/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<OrderWrapper> findAllCurrentOrderWrapper(@PathVariable String id){
        return orderWrapperService.findAllCurrentOrderWrapper(id);
    }

    @ResponseBody
    @RequestMapping(value="/findByOrderWrapper/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected OrderWrapper findByOrderWrapperId(@PathVariable String id){
        return orderWrapperService.findByOrderWrapperId(id);
    }
}
