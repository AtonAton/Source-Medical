package com.sourcemed.objects.resources;

import com.sourcemed.objects.objects.VitalSigns;
import com.sourcemed.objects.services.VitalSignsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
@Controller
@RequestMapping("/vitalSigns")
public class VitalSignsResource {
    @Autowired
    VitalSignsService vitalSignsService;

    @ResponseBody
    @RequestMapping(value="/findAllvitalSigns", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<VitalSigns> findAll(){
        return vitalSignsService.findAllVitalSigns();
    }

    @RequestMapping(value="/create", method = RequestMethod.POST)
    protected ResponseEntity<Void> createVitalSigns(@RequestBody VitalSigns vitalSigns){
        vitalSignsService.createVitalSigns(vitalSigns);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ResponseBody
    @RequestMapping(value="/findAllvitalSignsByPatientId/{patientId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<VitalSigns> findByPatientId(@PathVariable String patientId){
        return vitalSignsService.findAllVitalSignsByPatientId(patientId);
    }

    @RequestMapping(value="/checkIfIdExists/{id}", method=RequestMethod.POST)
    protected ResponseEntity<Void> idChecker(@PathVariable String id){
        VitalSigns vitalSigns = vitalSignsService.findByVitalSignsId(id);
        if(vitalSigns == null){
            return new ResponseEntity<>(HttpStatus.FOUND);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
