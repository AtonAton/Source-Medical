package com.sourcemed.objects.resources;
import com.sourcemed.objects.objects.MedicalRecord;
import com.sourcemed.objects.services.MedicalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
@Controller
@RequestMapping("/medicalRecords")
public class MedicalRecordResource {
    @Autowired
    MedicalRecordService medicalRecordService;

    @ResponseBody
    @RequestMapping(value = "/findAllMedRecords", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<MedicalRecord> findAllMedRecords(){
       return medicalRecordService.findAll();
    }

    @ResponseBody
    @RequestMapping(value = "/findMedRecByPatientId/{patientID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected MedicalRecord findMedRecByPatientId(@PathVariable String patientID){
        return medicalRecordService.findMedRecordByPatientId(patientID);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    protected ResponseEntity<Void> createMedicalRecord(@RequestBody MedicalRecord medicalRecord){
        medicalRecordService.createMedicalRecord(medicalRecord);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value="/checkIfIdExists/{id}", method=RequestMethod.POST)
    protected ResponseEntity<Void> idChecker(@PathVariable String id){
        MedicalRecord medicalRecord = medicalRecordService.findMedRecordByMedRecId(id);
        if(medicalRecord == null){
            return new ResponseEntity<>(HttpStatus.FOUND);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
