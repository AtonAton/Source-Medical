package com.sourcemed.objects.resources;

import com.sourcemed.objects.objects.SystemReview;
import com.sourcemed.objects.objects.VitalSigns;
import com.sourcemed.objects.services.SystemReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
@Controller
@RequestMapping("/systemReviews")
public class SystemReviewResource {
    @Autowired
    SystemReviewService systemReviewService;

    @ResponseBody
    @RequestMapping(value="/findAllSystemReviews", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<SystemReview> findAll(){
        return systemReviewService.findAllSystemReviews();
    }

    @RequestMapping(value="/create", method = RequestMethod.POST)
    protected ResponseEntity<Void> createSystemReview(@RequestBody SystemReview systemReview){
        systemReviewService.createSytemReview(systemReview);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ResponseBody
    @RequestMapping(value="/findAllSystemReviewByPatientId/{patientId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<SystemReview> findByPatientId(@PathVariable String patientId){
        return systemReviewService.findSystemReviewsByPatientId(patientId);
    }

    @RequestMapping(value="/checkIfIdExists/{id}", method=RequestMethod.POST)
    protected ResponseEntity<Void> idChecker(@PathVariable String id){
        SystemReview sysReview = systemReviewService.findBySystemReviewId(id);
        if(sysReview == null){
            return new ResponseEntity<>(HttpStatus.FOUND);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
