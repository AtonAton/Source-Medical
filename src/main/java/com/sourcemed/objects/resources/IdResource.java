package com.sourcemed.objects.resources;

import com.sourcemed.objects.objects.ID;
import com.sourcemed.objects.objects.Patient;
import com.sourcemed.objects.services.IdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */

@Controller
@RequestMapping("/ids")
public class IdResource {

    @Autowired
    IdService idService;

    @ResponseBody
    @RequestMapping(value="/findAllIds", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<ID> findAll(){
        return idService.findAllIds();
    }

    @RequestMapping(value="/createId", method= RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    protected ResponseEntity<Void> createId(@RequestBody ID id){
        idService.createId(id);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value="/checkIfIdExists/{id}", method=RequestMethod.POST)
    protected ResponseEntity<Void> idChecker(@PathVariable String id, @RequestBody Patient patient){
        String patientID = patient.getPatientId();
        List<ID> idsOfPatient = idService.findIdsByPatientId(patientID);
        for(int x =0; x<idsOfPatient.size();x++){
            if(idsOfPatient.get(x).getIdNumber().equals(id)){
                return new ResponseEntity<>(HttpStatus.ACCEPTED);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
    }

    @RequestMapping(value ="/findIdsByPatientId/{id}", method= RequestMethod.POST)
    protected ResponseEntity<Void> findIdsByPatientId(@PathVariable String id, @RequestBody Patient patient){
        String patient_id = patient.getPatientId();
        List<ID> ids = idService.findIdsByPatientId(patient_id);
        //System.out.print(ids.get(1));
        for(int x=0; x<ids.size(); x++){
            if(!ids.get(x).getIdNumber().equals(id)) {
                return new ResponseEntity<>(HttpStatus.FOUND);

            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
