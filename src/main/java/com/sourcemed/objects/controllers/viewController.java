package com.sourcemed.objects.controllers;

import com.sourcemed.objects.objects.Doctor;
import com.sourcemed.objects.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

/**
 * Created by PC on 9/2/2017.
 */
@Controller
public class viewController {

    @Autowired
    DoctorService doctorService;

    @RequestMapping(value="/", method = RequestMethod.GET)
    public String getIndex(){
        return "login";
    }

    @RequestMapping(value="/dashboard", method = RequestMethod.GET)
    public String getDashboard(){
        return "doctor_dashboard";
    }

    @RequestMapping(value="/test", method = RequestMethod.GET)
    public String getTest(){
        return "ViewPastClinicalSummary";
    }

    @RequestMapping(value="/viewDoctorsOrder", method = RequestMethod.GET)
    public String getOrder(){
        return "Add-HandleOrderPage";
    }

    /*@RequestMapping(value="/viewPastMedicalChart", method = RequestMethod.GET)
    public String getPastMedChart(){
        return "ViewPastClinicalSummary";
    }
*/
}
