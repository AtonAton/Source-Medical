package com.sourcemed.objects.objects;

import java.util.*;

/**
 * Created by Makoy on 7/19/2017.
 */
public class LaboratoryPersonnel {

    private String laboratoryPersonnelId;
    private String contributorId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String email;
    private String contactNumber;
    private String username;
    private String password;
    private String securityQuestion;
    private String securityAnswer;
    private List<MedicalInstitution> medicalInstitutionAffiliation;
    private List<Patient> patientsHandled;
    private List<MedicalChart> medicalChartsHandled;

    public String getContributorId() {
        return contributorId;
    }

    public void setContributorId(String contributorId) {
        this.contributorId = contributorId;
    }

    public String getLaboratoryPersonnelId() {
        return laboratoryPersonnelId;
    }

    public void setLaboratoryPersonnelId(String laboratoryPersonnelId) {
        this.laboratoryPersonnelId = laboratoryPersonnelId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    public List<MedicalInstitution> getMedicalInstitutionAffiliation() {
        return medicalInstitutionAffiliation;
    }

    public void setMedicalInstitutionAffiliation(List<MedicalInstitution> medicalInstitutionAffiliation) {
        this.medicalInstitutionAffiliation = medicalInstitutionAffiliation;
    }

    public List<Patient> getPatientsHandled() {
        return patientsHandled;
    }

    public void setPatientsHandled(List<Patient> patientsHandled) {
        this.patientsHandled = patientsHandled;
    }

    public List<MedicalChart> getMedicalChartsHandled() {
        return medicalChartsHandled;
    }

    public void setMedicalChartsHandled(List<MedicalChart> medicalChartsHandled) {
        this.medicalChartsHandled = medicalChartsHandled;
    }
}

