package com.sourcemed.objects.objects;

import java.util.List;

/**
<<<<<<< Updated upstream
 * Created by Cheeeng on 7/19/2017.
=======
 * Created by Makoy on 7/19/2017.
>>>>>>> Stashed changes
 */
public class MedicalInstitution extends BaseObject{

    private String medicalInstitution_Id;
    private String institutionName;
    private String institutionType; //Clinic,Hospital,etc.
    private String institutionClass; // Private,Public
    private String address;
    private String zipCode;
    private int longitude;
    private int latitude;
    private String contactNumber;
    private String email;
    private List<MedicalStaff> medicalStaffList;
    private List<Doctor> doctorList;
    private List<Nurse> nurseList;

    public String getMedicalInstitution_Id() {
        return medicalInstitution_Id;
    }

    public void setMedicalInstitution_Id(String medicalInstitution_Id) {
        this.medicalInstitution_Id = medicalInstitution_Id;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getInstitutionType() {
        return institutionType;
    }

    public void setInstitutionType(String institutionType) {
        this.institutionType = institutionType;
    }

    public String getInstitutionClass() {
        return institutionClass;
    }

    public void setInstitutionClass(String institutionClass) {
        this.institutionClass = institutionClass;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public int getLongitude() {
        return longitude;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public int getLatitude() {
        return latitude;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<MedicalStaff> getMedicalStaffList() {
        return medicalStaffList;
    }

    public void setMedicalStaffList(List<MedicalStaff> medicalStaffList) {
        this.medicalStaffList = medicalStaffList;
    }

    public List<Doctor> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(List<Doctor> doctorList) {
        this.doctorList = doctorList;
    }

    public List<Nurse> getNurseList() {
        return nurseList;
    }

    public void setNurseList(List<Nurse> nurseList) {
        this.nurseList = nurseList;
    }
}
