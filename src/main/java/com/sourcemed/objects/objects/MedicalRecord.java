package com.sourcemed.objects.objects;

import java.util.*;

/**
 * Created by Makoy on 7/24/2017.
 */
public class MedicalRecord {

    private String medicalRecordId;
    private String doctorId;
    private String patientId;
    private boolean pastMedicalHistoryAsthma;
    private boolean pastMedicalHistoryHypertension;
    private boolean pastMedicalHistoryCva;
    private boolean pastMedicalHistoryDiabetes;
    private boolean pastMedicalHistoryCancer;
    private boolean pastMedicalHistoryHeartDisease;
    private boolean pastMedicalHistoryLungDisease;
    private boolean pastMedicalHistoryLiverDisease;
    private boolean pastMedicalHistoryKidneyDisease;
    private List<String> otherDisease;
    private boolean familyAsthma;
    private boolean familyHypertension;
    private boolean familyCva;
    private boolean familyDiabetes;
    private boolean familyCancer;
    private boolean familyHeartDisease;
    private boolean familyLungDisease;
    private boolean familyLiverDisease;
    private boolean familyKidneyDisease;
    private List<String> familyOtherDisease;
    private boolean immunizationBacilleCalmetteGuerin;
    private boolean immunizationChickenpoxVaccine;
    private boolean immunizationDiphtheria;
    private boolean immunizationHepatitisAVaccine;
    private boolean immunizationHepatitisBVaccine;
    private boolean immunizationHibVaccine;
    private boolean immunizationHumanPappillomavirus;
    private boolean immunizationInfluenzaVaccine;
    private boolean immunizationMeasles;
    private boolean immunizationTetanus;
    private boolean immunizationPertussisVaccine;
    private boolean immunizationMumps;
    private boolean immunizationRubellaVaccine;
    private boolean immunizationMeningococcalVaccine;
    private boolean immunizationPneumococcalVaccine;
    private boolean immunizationPolioVaccine;
    private boolean immunizationRotarusVaccine;
    private List<String> otherVaccine;
    private List<MedicalChart> medicalCharts;

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getMedicalRecordId() {
        return medicalRecordId;
    }

    public void setMedicalRecordId(String medicalRecordId) {
        this.medicalRecordId = medicalRecordId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public boolean isPastMedicalHistoryAsthma() {
        return pastMedicalHistoryAsthma;
    }

    public void setPastMedicalHistoryAsthma(boolean pastMedicalHistoryAsthma) {
        this.pastMedicalHistoryAsthma = pastMedicalHistoryAsthma;
    }

    public boolean isPastMedicalHistoryHypertension() {
        return pastMedicalHistoryHypertension;
    }

    public void setPastMedicalHistoryHypertension(boolean pastMedicalHistoryHypertension) {
        this.pastMedicalHistoryHypertension = pastMedicalHistoryHypertension;
    }

    public boolean isPastMedicalHistoryCva() {
        return pastMedicalHistoryCva;
    }

    public void setPastMedicalHistoryCva(boolean pastMedicalHistoryCva) {
        this.pastMedicalHistoryCva = pastMedicalHistoryCva;
    }

    public boolean isPastMedicalHistoryDiabetes() {
        return pastMedicalHistoryDiabetes;
    }

    public void setPastMedicalHistoryDiabetes(boolean pastMedicalHistoryDiabetes) {
        this.pastMedicalHistoryDiabetes = pastMedicalHistoryDiabetes;
    }

    public boolean isPastMedicalHistoryCancer() {
        return pastMedicalHistoryCancer;
    }

    public void setPastMedicalHistoryCancer(boolean pastMedicalHistoryCancer) {
        this.pastMedicalHistoryCancer = pastMedicalHistoryCancer;
    }

    public boolean isPastMedicalHistoryHeartDisease() {
        return pastMedicalHistoryHeartDisease;
    }

    public void setPastMedicalHistoryHeartDisease(boolean pastMedicalHistoryHeartDisease) {
        this.pastMedicalHistoryHeartDisease = pastMedicalHistoryHeartDisease;
    }

    public boolean isPastMedicalHistoryLungDisease() {
        return pastMedicalHistoryLungDisease;
    }

    public void setPastMedicalHistoryLungDisease(boolean pastMedicalHistoryLungDisease) {
        this.pastMedicalHistoryLungDisease = pastMedicalHistoryLungDisease;
    }

    public boolean isPastMedicalHistoryLiverDisease() {
        return pastMedicalHistoryLiverDisease;
    }

    public void setPastMedicalHistoryLiverDisease(boolean pastMedicalHistoryLiverDisease) {
        this.pastMedicalHistoryLiverDisease = pastMedicalHistoryLiverDisease;
    }

    public boolean isPastMedicalHistoryKidneyDisease() {
        return pastMedicalHistoryKidneyDisease;
    }

    public void setPastMedicalHistoryKidneyDisease(boolean pastMedicalHistoryKidneyDisease) {
        this.pastMedicalHistoryKidneyDisease = pastMedicalHistoryKidneyDisease;
    }

    public List<String> getOtherDisease() {
        return otherDisease;
    }

    public void setOtherDisease(List<String> otherDisease) {
        this.otherDisease = otherDisease;
    }

    public boolean isFamilyAsthma() {
        return familyAsthma;
    }

    public void setFamilyAsthma(boolean familyAsthma) {
        this.familyAsthma = familyAsthma;
    }

    public boolean isFamilyHypertension() {
        return familyHypertension;
    }

    public void setFamilyHypertension(boolean familyHypertension) {
        this.familyHypertension = familyHypertension;
    }

    public boolean isFamilyCva() {
        return familyCva;
    }

    public void setFamilyCva(boolean familyCva) {
        this.familyCva = familyCva;
    }

    public boolean isFamilyDiabetes() {
        return familyDiabetes;
    }

    public void setFamilyDiabetes(boolean familyDiabetes) {
        this.familyDiabetes = familyDiabetes;
    }

    public boolean isFamilyCancer() {
        return familyCancer;
    }

    public void setFamilyCancer(boolean familyCancer) {
        this.familyCancer = familyCancer;
    }

    public boolean isFamilyHeartDisease() {
        return familyHeartDisease;
    }

    public void setFamilyHeartDisease(boolean familyHeartDisease) {
        this.familyHeartDisease = familyHeartDisease;
    }

    public boolean isFamilyLungDisease() {
        return familyLungDisease;
    }

    public void setFamilyLungDisease(boolean familyLungDisease) {
        this.familyLungDisease = familyLungDisease;
    }

    public boolean isFamilyLiverDisease() {
        return familyLiverDisease;
    }

    public void setFamilyLiverDisease(boolean familyLiverDisease) {
        this.familyLiverDisease = familyLiverDisease;
    }

    public boolean isFamilyKidneyDisease() {
        return familyKidneyDisease;
    }

    public void setFamilyKidneyDisease(boolean familyKidneyDisease) {
        this.familyKidneyDisease = familyKidneyDisease;
    }

    public List<String> getFamilyOtherDisease() {
        return familyOtherDisease;
    }

    public void setFamilyOtherDisease(List<String> familyOtherDisease) {
        this.familyOtherDisease = familyOtherDisease;
    }

    public boolean isImmunizationBacilleCalmetteGuerin() {
        return immunizationBacilleCalmetteGuerin;
    }

    public void setImmunizationBacilleCalmetteGuerin(boolean immunizationBacilleCalmetteGuerin) {
        this.immunizationBacilleCalmetteGuerin = immunizationBacilleCalmetteGuerin;
    }

    public boolean isImmunizationChickenpoxVaccine() {
        return immunizationChickenpoxVaccine;
    }

    public void setImmunizationChickenpoxVaccine(boolean immunizationChickenpoxVaccine) {
        this.immunizationChickenpoxVaccine = immunizationChickenpoxVaccine;
    }

    public boolean isImmunizationDiphtheria() {
        return immunizationDiphtheria;
    }

    public void setImmunizationDiphtheria(boolean immunizationDiphtheria) {
        this.immunizationDiphtheria = immunizationDiphtheria;
    }

    public boolean isImmunizationHepatitisAVaccine() {
        return immunizationHepatitisAVaccine;
    }

    public void setImmunizationHepatitisAVaccine(boolean immunizationHepatitisAVaccine) {
        this.immunizationHepatitisAVaccine = immunizationHepatitisAVaccine;
    }

    public boolean isImmunizationHepatitisBVaccine() {
        return immunizationHepatitisBVaccine;
    }

    public void setImmunizationHepatitisBVaccine(boolean immunizationHepatitisBVaccine) {
        this.immunizationHepatitisBVaccine = immunizationHepatitisBVaccine;
    }

    public boolean isImmunizationHibVaccine() {
        return immunizationHibVaccine;
    }

    public void setImmunizationHibVaccine(boolean immunizationHibVaccine) {
        this.immunizationHibVaccine = immunizationHibVaccine;
    }

    public boolean isImmunizationHumanPappillomavirus() {
        return immunizationHumanPappillomavirus;
    }

    public void setImmunizationHumanPappillomavirus(boolean immunizationHumanPappillomavirus) {
        this.immunizationHumanPappillomavirus = immunizationHumanPappillomavirus;
    }

    public boolean isImmunizationInfluenzaVaccine() {
        return immunizationInfluenzaVaccine;
    }

    public void setImmunizationInfluenzaVaccine(boolean immunizationInfluenzaVaccine) {
        this.immunizationInfluenzaVaccine = immunizationInfluenzaVaccine;
    }

    public boolean isImmunizationMeasles() {
        return immunizationMeasles;
    }

    public void setImmunizationMeasles(boolean immunizationMeasles) {
        this.immunizationMeasles = immunizationMeasles;
    }

    public boolean isImmunizationTetanus() {
        return immunizationTetanus;
    }

    public void setImmunizationTetanus(boolean immunizationTetanus) {
        this.immunizationTetanus = immunizationTetanus;
    }

    public boolean isImmunizationPertussisVaccine() {
        return immunizationPertussisVaccine;
    }

    public void setImmunizationPertussisVaccine(boolean immunizationPertussisVaccine) {
        this.immunizationPertussisVaccine = immunizationPertussisVaccine;
    }

    public boolean isImmunizationMumps() {
        return immunizationMumps;
    }

    public void setImmunizationMumps(boolean immunizationMumps) {
        this.immunizationMumps = immunizationMumps;
    }

    public boolean isImmunizationRubellaVaccine() {
        return immunizationRubellaVaccine;
    }

    public void setImmunizationRubellaVaccine(boolean immunizationRubellaVaccine) {
        this.immunizationRubellaVaccine = immunizationRubellaVaccine;
    }

    public boolean isImmunizationMeningococcalVaccine() {
        return immunizationMeningococcalVaccine;
    }

    public void setImmunizationMeningococcalVaccine(boolean immunizationMeningococcalVaccine) {
        this.immunizationMeningococcalVaccine = immunizationMeningococcalVaccine;
    }

    public boolean isImmunizationPneumococcalVaccine() {
        return immunizationPneumococcalVaccine;
    }

    public void setImmunizationPneumococcalVaccine(boolean immunizationPneumococcalVaccine) {
        this.immunizationPneumococcalVaccine = immunizationPneumococcalVaccine;
    }

    public boolean isImmunizationPolioVaccine() {
        return immunizationPolioVaccine;
    }

    public void setImmunizationPolioVaccine(boolean immunizationPolioVaccine) {
        this.immunizationPolioVaccine = immunizationPolioVaccine;
    }

    public boolean isImmunizationRotarusVaccine() {
        return immunizationRotarusVaccine;
    }

    public void setImmunizationRotarusVaccine(boolean immunizationRotarusVaccine) {
        this.immunizationRotarusVaccine = immunizationRotarusVaccine;
    }

    public List<String> getOtherVaccine() {
        return otherVaccine;
    }

    public void setOtherVaccine(List<String> otherVaccine) {
        this.otherVaccine = otherVaccine;
    }

    public List<MedicalChart> getMedicalCharts() {
        return medicalCharts;
    }

    public void setMedicalCharts(List<MedicalChart> medicalCharts) {
        this.medicalCharts = medicalCharts;
    }
}
