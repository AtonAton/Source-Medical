package com.sourcemed.objects.objects;

import org.springframework.data.annotation.Id;

import java.util.*;

/**
 * Created by Makoy on 7/24/2017.
 */
public class MedicalChart {
    @Id
    private String _Id;
    private String AdmittingDoctor;
    private String medicalChartId;
    private String medicalRecordId;
    private String patientId;
    private boolean isCurrent;
    private Date date;
    private List<Doctor> attendingDoctorList;
    private List<MedicalStaff> attendingMedicalStaffList;
    private List<LaboratoryResult> laboratoryTestResultList;
    private List<SystemReview> systemReviewList;
    private List<PhysicalExam> physicalExamList;
    private List<VitalSigns> vitalSignsList;
    private boolean historyPolyuria;
    private boolean historyPolydipsia;
    private boolean historyBlurredVision;
    private boolean historyDiaphoresis;
    private boolean historyAgitation;
    private boolean historyTremor;
    private boolean historyPalpitations;
    private boolean historyInsomia;
    private boolean historyConfusion;
    private boolean historyLethargy;
    private boolean historySomnolence;
    private boolean historyAmnesia;
    private boolean historyStupor;
    private boolean historySeizures;
    private List<String> medications;
    private List<String> operations;
    private List<String> labExams;
    private List<String> services;
    private String summary;
    private String treatment;
    private List<DoctorsOrder> orders;
    private String counsellingTime;
    private String coordinationOfCaretime;
    private String returnVisit;
    private String disposition;
    private String reasonForAdmission;
    private String primaryDiagnosis;
    private String differentialDiagnosis;

    public String get_Id() {
        return _Id;
    }

    public void set_Id(String _Id) {
        this._Id = _Id;
    }

    public List<String> getMedications() {
        return medications;
    }

    public List<String> getLabExams() {
        return labExams;
    }

    public void setLabExams(List<String> labExams) {
        this.labExams = labExams;
    }

    public void setMedications(List<String> medications) {
        this.medications = medications;
    }

    public List<String> getOperations() {
        return operations;
    }

    public void setOperations(List<String> operations) {
        this.operations = operations;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getPrimaryDiagnosis() {
        return primaryDiagnosis;
    }

    public void setPrimaryDiagnosis(String primaryDiagnosis) {
        this.primaryDiagnosis = primaryDiagnosis;
    }

    public String getDifferentialDiagnosis() {
        return differentialDiagnosis;
    }

    public void setDifferentialDiagnosis(String differentialDiagnosis) {
        this.differentialDiagnosis = differentialDiagnosis;
    }

    public String getReasonForAdmission() {
        return reasonForAdmission;
    }

    public void setReasonForAdmission(String reasonForAdmission) {
        this.reasonForAdmission = reasonForAdmission;
    }

    public String getAdmittingDoctor() {
        return AdmittingDoctor;
    }

    public void setAdmittingDoctor(String admittingDoctor) {
        AdmittingDoctor = admittingDoctor;
    }

    public String getMedicalChartId() {
        return medicalChartId;
    }

    public void setMedicalChartId(String medicalChartId) {
        this.medicalChartId = medicalChartId;
    }

    public String getMedicalRecordId() {
        return medicalRecordId;
    }

    public void setMedicalRecordId(String medicalRecordID) {
        this.medicalRecordId = medicalRecordID;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Doctor> getAttendingDoctorList() {
        return attendingDoctorList;
    }

    public void setAttendingDoctorList(List<Doctor> attendingDoctorList) {
        this.attendingDoctorList = attendingDoctorList;
    }

    public List<MedicalStaff> getAttendingMedicalStaffList() {
        return attendingMedicalStaffList;
    }

    public void setAttendingMedicalStaffList(List<MedicalStaff> attendingMedicalStaffList) {
        this.attendingMedicalStaffList = attendingMedicalStaffList;
    }

    public List<LaboratoryResult> getLaboratoryTestResultList() {
        return laboratoryTestResultList;
    }

    public void setLaboratoryTestResultList(List<LaboratoryResult> laboratoryTestResultList) {
        this.laboratoryTestResultList = laboratoryTestResultList;
    }

    public List<SystemReview> getSystemReviewList() {
        return systemReviewList;
    }

    public void setSystemReviewList(List<SystemReview> systemReviewList) {
        this.systemReviewList = systemReviewList;
    }

    public List<PhysicalExam> getPhysicalExamList() {
        return physicalExamList;
    }

    public void setPhysicalExamList(List<PhysicalExam> physicalExamList) {
        this.physicalExamList = physicalExamList;
    }

    public List<VitalSigns> getVitalSignsList() {
        return vitalSignsList;
    }

    public void setVitalSignsList(List<VitalSigns> vitalSignsList) {
        this.vitalSignsList = vitalSignsList;
    }

    public boolean isHistoryPolyuria() {
        return historyPolyuria;
    }

    public void setHistoryPolyuria(boolean historyPolyuria) {
        this.historyPolyuria = historyPolyuria;
    }

    public boolean isHistoryPolydipsia() {
        return historyPolydipsia;
    }

    public void setHistoryPolydipsia(boolean historyPolydipsia) {
        this.historyPolydipsia = historyPolydipsia;
    }

    public boolean isHistoryBlurredVision() {
        return historyBlurredVision;
    }

    public void setHistoryBlurredVision(boolean historyBlurredVision) {
        this.historyBlurredVision = historyBlurredVision;
    }

    public boolean isHistoryDiaphoresis() {
        return historyDiaphoresis;
    }

    public void setHistoryDiaphoresis(boolean historyDiaphoresis) {
        this.historyDiaphoresis = historyDiaphoresis;
    }

    public boolean isHistoryAgitation() {
        return historyAgitation;
    }

    public void setHistoryAgitation(boolean historyAgitation) {
        this.historyAgitation = historyAgitation;
    }

    public boolean isHistoryTremor() {
        return historyTremor;
    }

    public void setHistoryTremor(boolean historyTremor) {
        this.historyTremor = historyTremor;
    }

    public boolean isHistoryPalpitations() {
        return historyPalpitations;
    }

    public void setHistoryPalpitations(boolean historyPalpitations) {
        this.historyPalpitations = historyPalpitations;
    }

    public boolean isHistoryInsomia() {
        return historyInsomia;
    }

    public void setHistoryInsomia(boolean historyInsomia) {
        this.historyInsomia = historyInsomia;
    }

    public boolean isHistoryConfusion() {
        return historyConfusion;
    }

    public void setHistoryConfusion(boolean historyConfusion) {
        this.historyConfusion = historyConfusion;
    }

    public boolean isHistoryLethargy() {
        return historyLethargy;
    }

    public void setHistoryLethargy(boolean historyLethargy) {
        this.historyLethargy = historyLethargy;
    }

    public boolean isHistorySomnolence() {
        return historySomnolence;
    }

    public void setHistorySomnolence(boolean historySomnolence) {
        this.historySomnolence = historySomnolence;
    }

    public boolean isHistoryAmnesia() {
        return historyAmnesia;
    }

    public void setHistoryAmnesia(boolean historyAmnesia) {
        this.historyAmnesia = historyAmnesia;
    }

    public boolean isHistoryStupor() {
        return historyStupor;
    }

    public void setHistoryStupor(boolean historyStupor) {
        this.historyStupor = historyStupor;
    }

    public boolean isHistorySeizures() {
        return historySeizures;
    }

    public void setHistorySeizures(boolean historySeizures) {
        this.historySeizures = historySeizures;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public List<DoctorsOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<DoctorsOrder> orders) {
        this.orders = orders;
    }

    public String getCounsellingTime() {
        return counsellingTime;
    }

    public void setCounsellingTime(String counsellingTime) {
        this.counsellingTime = counsellingTime;
    }

    public String getCoordinationOfCaretime() {
        return coordinationOfCaretime;
    }

    public void setCoordinationOfCaretime(String coordinationOfCaretime) {
        this.coordinationOfCaretime = coordinationOfCaretime;
    }

    public String getReturnVisit() {
        return returnVisit;
    }

    public void setReturnVisit(String returnVisit) {
        this.returnVisit = returnVisit;
    }

    public String getDisposition() {
        return disposition;
    }

    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }
}
