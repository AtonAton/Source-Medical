package com.sourcemed.objects.objects;

import java.util.Date;
import java.util.List;

/**
 * Created by Cheeeng on 10/10/2017.
 */
public class OrderWrapper {

    private String medicalChartId;
    private String orderWrapperId;
    private String patientId;
    private Date creationDate;
    private List<DoctorsOrder> orders;
    private String doctorId;
    private boolean isCurrent;
    private String createdBy;
    private String specialization;

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getMedicalChartId() {
        return medicalChartId;
    }

    public void setMedicalChartId(String medicalChartId) {
        this.medicalChartId = medicalChartId;
    }

    public String getOrderWrapperId() {
        return orderWrapperId;
    }

    public void setOrderWrapperId(String orderWrapperId) {
        this.orderWrapperId = orderWrapperId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<DoctorsOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<DoctorsOrder> orders) {
        this.orders = orders;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
    }


    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
