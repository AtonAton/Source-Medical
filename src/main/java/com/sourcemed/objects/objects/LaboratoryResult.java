package com.sourcemed.objects.objects;

import java.sql.Time;
import java.util.Date;

/**
 * Created by Makoy on 7/19/2017.
 */
public class LaboratoryResult {

    private String laboratoryResult_Id;
    private String patientId;
    private String medicalChartId;
    private Date dateOfTest;
    private String timeOfTest;
    private String laboratoryExamType;
    private String laboratoryPersonnel_ID;
    private String results;
    private String contributor;

    public String getContributor() {
        return contributor;
    }

    public void setContributor(String contributor) {
        this.contributor = contributor;
    }

    public String getLaboratoryResult_Id() {
        return laboratoryResult_Id;
    }

    public void setLaboratoryResult_Id(String laboratoryResult_Id) {
        this.laboratoryResult_Id = laboratoryResult_Id;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getMedicalChartId() {
        return medicalChartId;
    }

    public void setMedicalChartId(String medicalChart_Id) {
        this.medicalChartId = medicalChartId;
    }

    public Date getDateOfTest() {
        return dateOfTest;
    }

    public void setDateOfTest(Date dateOfTest) {
        this.dateOfTest = dateOfTest;
    }

    public String getTimeOfTest() {
        return timeOfTest;
    }

    public void setTimeOfTest(String timeOfTest) {
        this.timeOfTest = timeOfTest;
    }

    public String getLaboratoryExamType() {
        return laboratoryExamType;
    }

    public void setLaboratoryExamType(String laboratoryExamType) {
        this.laboratoryExamType = laboratoryExamType;
    }

    public String getLaboratoryPersonnel_ID() {
        return laboratoryPersonnel_ID;
    }

    public void setLaboratoryPersonnel_ID(String laboratoryPersonnel_ID) {
        this.laboratoryPersonnel_ID = laboratoryPersonnel_ID;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }
}



