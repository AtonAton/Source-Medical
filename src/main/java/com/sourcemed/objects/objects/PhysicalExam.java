package com.sourcemed.objects.objects;

import java.util.Date;

/**
 * Created by Makoy on 7/24/2017.
 */
public class PhysicalExam {
    private String medicalChartId;
    private String physicalExamId;
    private String patientId;
    private Date date;
    private String conductorID;
    private String contributor;
    private String generalApperance;
    private String eyes;
    private String nose;
    private String ears;
    private String mouth;
    private String throat;
    private String respiratory;
    private String cardioVascular;
    private String skin;
    private String problem;
    private String impression;
    private String notes;
    private boolean isCurrent;

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
    }

    public String getContributor() {
        return contributor;
    }

    public void setContributor(String contributor) {
        this.contributor = contributor;
    }

    public String getMedicalChartId() {
        return medicalChartId;
    }

    public void setMedicalChartId(String medicalChartId) {
        this.medicalChartId = medicalChartId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPhysicalExamId() {
        return physicalExamId;
    }

    public void setPhysicalExamId(String physicalExamId) {
        this.physicalExamId = physicalExamId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getConductorID() {
        return conductorID;
    }

    public void setConductorID(String conductorID) {
        this.conductorID = conductorID;
    }

    public String getGeneralApperance() {
        return generalApperance;
    }

    public void setGeneralApperance(String generalApperance) {
        this.generalApperance = generalApperance;
    }

    public String getEyes() {
        return eyes;
    }

    public void setEyes(String eyes) {
        this.eyes = eyes;
    }

    public String getNose() {
        return nose;
    }

    public void setNose(String nose) {
        this.nose = nose;
    }

    public String getEars() {
        return ears;
    }

    public void setEars(String ears) {
        this.ears = ears;
    }

    public String getMouth() {
        return mouth;
    }

    public void setMouth(String mouth) {
        this.mouth = mouth;
    }

    public String getThroat() {
        return throat;
    }

    public void setThroat(String throat) {
        this.throat = throat;
    }

    public String getRespiratory() {
        return respiratory;
    }

    public void setRespiratory(String respiratory) {
        this.respiratory = respiratory;
    }

    public String getCardioVascular() {
        return cardioVascular;
    }

    public void setCardioVascular(String cardioVascular) {
        this.cardioVascular = cardioVascular;
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getImpression() {
        return impression;
    }

    public void setImpression(String impression) {
        this.impression = impression;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
