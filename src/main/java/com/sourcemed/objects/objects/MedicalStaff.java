package com.sourcemed.objects.objects;

import java.util.List;

/**
 * Created by Makoy on 7/19/2017.
 */
public class MedicalStaff {

    private String medicalStaffId;
    private String contributorId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String specialization;
    private String email;
    private String contactNumber;
    private String username;
    private String password;
    private String securityQuestion;
    private String securityAnswer;
    private String medicalInstitution;
    private List<Patient> patientsHandledList;
    private List<MedicalChart> medicalChartHandledList;
    private List<Notifications> notifications;
    private List<DoctorsOrder> orders;

    public String getContributorId() {
        return contributorId;
    }

    public void setContributorId(String contributorId) {
        this.contributorId = contributorId;
    }

    public String getMedicalStaffId() {
        return medicalStaffId;
    }

    public void setMedicalStaffId(String medicalStaffId) {
        this.medicalStaffId = medicalStaffId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    public String getMedicalInstitution() {
        return medicalInstitution;
    }

    public void setMedicalInstitution(String medicalInstitution) {
        this.medicalInstitution = medicalInstitution;
    }

    public List<Patient> getPatientsHandledList() {
        return patientsHandledList;
    }

    public void setPatientsHandledList(List<Patient> patientsHandledList) {
        this.patientsHandledList = patientsHandledList;
    }

    public List<MedicalChart> getMedicalChartHandledList() {
        return medicalChartHandledList;
    }

    public void setMedicalChartHandledList(List<MedicalChart> medicalChartHandledList) {
        this.medicalChartHandledList = medicalChartHandledList;
    }

    public List<Notifications> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notifications> notifications) {
        this.notifications = notifications;
    }

    public List<DoctorsOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<DoctorsOrder> orders) {
        this.orders = orders;
    }
}
