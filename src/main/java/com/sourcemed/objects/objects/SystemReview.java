package com.sourcemed.objects.objects;

import java.util.Date;

/**
 * Created by Makoy on 7/24/2017.
 */
public class SystemReview {
    private String medicalChartId;
    private String patientId;
    private String systemReviewId;
    private Date date;
    private String conductorId;
    private String contributor;
    private String general;
    private String eyes;
    private String nose;
    private String ears;
    private String throat;
    private String cardioVascular;
    private String gastrointestinal;
    private String genitourinary;
    private String musculoskeletal;
    private String skin;
    private String neurologic;
    private String psychiatric;
    private String endocrine;
    private String hemelympathic;
    private String allergicimmunologic;
    private String notes;
    private boolean isCurrent;

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
    }

    public String getContributor() {
        return contributor;
    }

    public void setContributor(String contributor) {
        this.contributor = contributor;
    }

    public String getMedicalChartId() {
        return medicalChartId;
    }

    public void setMedicalChartId(String medicalChartId) {
        this.medicalChartId = medicalChartId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getSystemReviewId() {
        return systemReviewId;
    }

    public void setSystemReviewId(String systemReviewId) {
        this.systemReviewId = systemReviewId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getConductorId() {
        return conductorId;
    }

    public void setConductorId(String conductorId) {
        this.conductorId = conductorId;
    }

    public String getGeneral() {
        return general;
    }

    public void setGeneral(String general) {
        this.general = general;
    }

    public String getEyes() {
        return eyes;
    }

    public void setEyes(String eyes) {
        this.eyes = eyes;
    }

    public String getNose() {
        return nose;
    }

    public void setNose(String nose) {
        this.nose = nose;
    }

    public String getEars() {
        return ears;
    }

    public void setEars(String ears) {
        this.ears = ears;
    }

    public String getThroat() {
        return throat;
    }

    public void setThroat(String throat) {
        this.throat = throat;
    }

    public String getCardioVascular() {
        return cardioVascular;
    }

    public void setCardioVascular(String cardioVascular) {
        this.cardioVascular = cardioVascular;
    }

    public String getGastrointestinal() {
        return gastrointestinal;
    }

    public void setGastrointestinal(String gastrointestinal) {
        this.gastrointestinal = gastrointestinal;
    }

    public String getGenitourinary() {
        return genitourinary;
    }

    public void setGenitourinary(String genitourinary) {
        this.genitourinary = genitourinary;
    }

    public String getMusculoskeletal() {
        return musculoskeletal;
    }

    public void setMusculoskeletal(String musculoskeletal) {
        this.musculoskeletal = musculoskeletal;
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getNeurologic() {
        return neurologic;
    }

    public void setNeurologic(String neurologic) {
        this.neurologic = neurologic;
    }

    public String getPsychiatric() {
        return psychiatric;
    }

    public void setPsychiatric(String psychiatric) {
        this.psychiatric = psychiatric;
    }

    public String getEndocrine() {
        return endocrine;
    }

    public void setEndocrine(String endocrine) {
        this.endocrine = endocrine;
    }

    public String getHemelympathic() {
        return hemelympathic;
    }

    public void setHemelympathic(String hemelympathic) {
        this.hemelympathic = hemelympathic;
    }

    public String getAllergicimmunologic() {
        return allergicimmunologic;
    }

    public void setAllergicimmunologic(String allergicimmunologic) {
        this.allergicimmunologic = allergicimmunologic;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
