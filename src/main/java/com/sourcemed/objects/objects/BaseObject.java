package com.sourcemed.objects.objects;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by PC on 8/14/2017.
 */
public class BaseObject implements Serializable {


    private String id;
    private String created_at;
    private String updated_at;
    private boolean isActive;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
