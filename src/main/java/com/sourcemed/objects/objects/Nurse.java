package com.sourcemed.objects.objects;

import java.util.Date;
import java.util.List;

/**
 * Created by PC on 7/25/2017.
 */
public class Nurse {
    private String nurseId;
    private String contributorId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private Date birthDate;
    private String expertise;
    private String email;
    private String contactNumber;
    private String username;
    private String password;
    private String securityQuestion;
    private String securityAnswer;
    private List<MedicalInstitution> medicalInstitution;
    private List<MedicalChart> medicalChartsHandled;
    private List<Patient> patientsHandled;

    public String getContributorId() {
        return contributorId;
    }

    public void setContributorId(String contributorId) {
        this.contributorId = contributorId;
    }

    public String getNurseId() {
        return nurseId;
    }

    public void setNurseId(String nurseId) {
        this.nurseId = nurseId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getExpertise() {
        return expertise;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    public List<MedicalInstitution> getMedicalInstitution() {
        return medicalInstitution;
    }

    public void setMedicalInstitution(List<MedicalInstitution> medicalInstitution) {
        this.medicalInstitution = medicalInstitution;
    }

    public List<MedicalChart> getMedicalChartsHandled() {
        return medicalChartsHandled;
    }

    public void setMedicalChartsHandled(List<MedicalChart> medicalChartsHandled) {
        this.medicalChartsHandled = medicalChartsHandled;
    }

    public List<Patient> getPatientsHandled() {
        return patientsHandled;
    }

    public void setPatientsHandled(List<Patient> patientsHandled) {
        this.patientsHandled = patientsHandled;
    }
}
