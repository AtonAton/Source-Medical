package com.sourcemed.objects.objects;

import java.util.Date;

/**
 * Created by PC on 8/3/2017.
 */
public class Notifications {

    private String notification_Id;
    private Date date;
    private String time;
    private String subjectLine;
    private String message;
    private String reciepient_Id;

    public String getNotification_Id() {
        return notification_Id;
    }

    public void setNotification_Id(String notification_Id) {
        this.notification_Id = notification_Id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSubjectLine() {
        return subjectLine;
    }

    public void setSubjectLine(String subjectLine) {
        this.subjectLine = subjectLine;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReciepient_Id() {
        return reciepient_Id;
    }

    public void setReciepient_Id(String reciepient_Id) {
        this.reciepient_Id = reciepient_Id;
    }
}
