package com.sourcemed.objects.objects;

import java.util.Date;

/**
 * Created by PC on 7/25/2017.
 */
public class VitalSigns {

    private String vitalSignsId;
    private String medicalChartId;
    private String patientId;
    private Date collectionDate;
    private String contributorId;
    private String contributor;
    private double heightCm;
    private double weightKg;
    private double temperatureCelcius;
    private String temperatureSite;
    private int pulsePerMinute;
    private String pulseType;
    private int respirationPerMinute;
    private String bloodPressure;
    private String notes;
    private boolean isCurrent;

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
    }

    public String getContributor() {
        return contributor;
    }

    public void setContributor(String contributor) {
        this.contributor = contributor;
    }

    public String getMedicalChartId() {
        return medicalChartId;
    }

    public void setMedicalChartId(String medicalChartId) {
        this.medicalChartId = medicalChartId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getVitalSignsId() {
        return vitalSignsId;
    }

    public void setVitalSignsId(String vitalSignsId) {
        this.vitalSignsId = vitalSignsId;
    }

    public Date getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(Date collectionDate) {
        this.collectionDate = collectionDate;
    }

    public String getContributorId() {
        return contributorId;
    }

    public void setContributorId(String contributorId) {
        this.contributorId = contributorId;
    }

    public double getHeightCm() {
        return heightCm;
    }

    public void setHeightCm(double heightCm) {
        this.heightCm = heightCm;
    }

    public double getWeightKg() {
        return weightKg;
    }

    public void setWeightKg(double weightKg) {
        this.weightKg = weightKg;
    }

    public double getTemperatureCelcius() {
        return temperatureCelcius;
    }

    public void setTemperatureCelcius(double temperatureCelcius) {
        this.temperatureCelcius = temperatureCelcius;
    }

    public String getTemperatureSite() {
        return temperatureSite;
    }

    public void setTemperatureSite(String temperatureSite) {
        this.temperatureSite = temperatureSite;
    }

    public int getPulsePerMinute() {
        return pulsePerMinute;
    }

    public void setPulsePerMinute(int pulsePerMinute) {
        this.pulsePerMinute = pulsePerMinute;
    }

    public String getPulseType() {
        return pulseType;
    }

    public void setPulseType(String pulseType) {
        this.pulseType = pulseType;
    }

    public int getRespirationPerMinute() {
        return respirationPerMinute;
    }

    public void setRespirationPerMinute(int respirationPerMinute) {
        this.respirationPerMinute = respirationPerMinute;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
