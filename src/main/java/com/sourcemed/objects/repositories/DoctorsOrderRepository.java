package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.DoctorsOrder;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
public interface DoctorsOrderRepository extends MongoRepository<DoctorsOrder, String> {
    List<DoctorsOrder> findAllDoctorsOrdersBypatientId(String patientId);
    List<DoctorsOrder> findAllBymedicalChartId(String medicalChartId);
    DoctorsOrder findDoctorsOrderByorderId(String orderId);
    List<DoctorsOrder> findByorderWrapperId(String orderWrapperId);
}
