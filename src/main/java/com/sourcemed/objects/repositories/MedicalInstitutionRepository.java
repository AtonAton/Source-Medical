package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.MedicalInstitution;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * Created by PC on 8/5/2017.
 */
public interface MedicalInstitutionRepository extends MongoRepository<MedicalInstitution, String> {

    @Query("{ 'medicalInstitution_Id' : ?0 }")
    MedicalInstitution findById(String medicalInstitution_Id);
}
