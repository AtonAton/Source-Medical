package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.SystemReview;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
public interface SystemReviewRepository extends MongoRepository<SystemReview, String> {
    List<SystemReview> findBypatientId(String patientId);
    SystemReview findBysystemReviewId(String systemReviewId);
}
