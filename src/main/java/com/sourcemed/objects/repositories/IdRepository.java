package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.ID;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
public interface IdRepository extends MongoRepository<ID, String> {
    List<ID> findIdsBypatientId(String patientId);
}
