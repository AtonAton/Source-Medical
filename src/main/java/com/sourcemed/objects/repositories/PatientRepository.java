package com.sourcemed.objects.repositories;


import com.sourcemed.objects.objects.Patient;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface PatientRepository extends MongoRepository<Patient, String>{
    Patient findById(String patient_Id);
    Patient findBypatientId(String patientId);
}
