package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.LaboratoryPersonnel;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by PC on 8/5/2017.
 */
public interface LaboratoryPersonnelRepository extends MongoRepository<LaboratoryPersonnel, String>{
}
