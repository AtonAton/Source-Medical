package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.Doctor;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
public interface DoctorRepository extends MongoRepository<Doctor, String> {
    Doctor findById(String doctor_id);
    Doctor findBydoctorId(String doctorId);
    Doctor findByusername(String username);
    List<Doctor> findByLastName(String lastName);
}
