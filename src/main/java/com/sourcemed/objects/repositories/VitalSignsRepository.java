package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.VitalSigns;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
public interface VitalSignsRepository extends MongoRepository<VitalSigns, String>{
    List<VitalSigns> findVitalSignsBypatientId(String patientId);
    VitalSigns findByvitalSignsId(String vitalSignsId);
}
