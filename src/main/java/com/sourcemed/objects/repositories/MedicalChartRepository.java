package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.MedicalChart;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
public interface MedicalChartRepository extends MongoRepository<MedicalChart, String> {
    List<MedicalChart> findByPatientId(String patient_Id);
    MedicalChart findByMedicalChartId(String medicalChartId);
}
