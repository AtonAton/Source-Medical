package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.DoctorsOrder;
import com.sourcemed.objects.objects.OrderWrapper;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by Cheeeng on 10/10/2017.
 */
public interface OrderWrapperRepository extends MongoRepository<OrderWrapper, String>{
    OrderWrapper findOrderWrapperByorderWrapperId(String orderWrapperId);
    List<OrderWrapper> findBypatientId(String patientId);

}
