package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.MedicalRecord;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
public interface MedicalRecordRepository extends MongoRepository<MedicalRecord, String> {
    List<MedicalRecord> findMedicalRecordsBydoctorId(String doctorId);
    MedicalRecord findMedRecordByPatientId(String patientId);
    MedicalRecord findByMedicalRecordId(String medicalRecordId);
}
