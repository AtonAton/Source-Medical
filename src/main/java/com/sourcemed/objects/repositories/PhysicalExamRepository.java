package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.PhysicalExam;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
public interface PhysicalExamRepository extends MongoRepository<PhysicalExam, String> {
    List<PhysicalExam> findAllBypatientId(String patientId);
    PhysicalExam findPhysicalExamByphysicalExamId(String physicalExamId);
}
