package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.LaboratoryResult;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
public interface LaboratoryResultRepository extends MongoRepository<LaboratoryResult, String> {
    List<LaboratoryResult> findIdsBypatientId(String patientId);
}

