package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.MedicalStaff;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by PC on 8/5/2017.
 */
public interface MedicalStaffRepository extends MongoRepository<MedicalStaff, String>{
    MedicalStaff findBymedicalStaffId(String medicalStaffId);
}
