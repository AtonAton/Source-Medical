package com.sourcemed.objects.repositories;

import com.sourcemed.objects.objects.Nurse;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by PC on 8/5/2017.
 */
public interface NurseRepository extends MongoRepository<Nurse, String> {
}
