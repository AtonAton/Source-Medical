'use strict'
var dashboard = angular.module('sourceMedApp');

dashboard.controller('dashboardController', function(LoginService, $scope, $http, $rootScope, $cookies){
    var doctorUserId = $cookies.get('username');
    var doctorPassword = $cookies.get('passWord');

    getLoggedInUser(doctorUserId,doctorPassword);
    function getLoggedInUser(uId,pword) {
        LoginService.getLoggedInUser(uId,pword)
            .then(function (loggedInUser) {
                $rootScope.doctor = loggedInUser;
            },
            function (errorMessage) {
                console.log(errorMessage);
            });
    }
});