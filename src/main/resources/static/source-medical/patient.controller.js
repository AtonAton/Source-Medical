'use strict'
var patients = angular.module('sourceMedApp');

patients.service("UniqueIdService", function($http){

    var self = this;
    var nextId = "";
    var possible = "ABCDEFGHIkLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    self.getUniqueId = function(){
        nextId="";
        for (var i = 0; i < 20; i++) {
            nextId += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return nextId;
        $http.post('http://localhost:8080/patients/checkIfIdExists/'+nextId).then(function(response){
            return nextId;

        },function(response){
            nextId="";
            self.getUniqueId();
        });

    }
});
patients.service("UniqueMedRecIdService", function($http){

    var self = this;
    var nextId = "";
    var possible = "ABCDEFGHIkLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    self.getUniqueId = function(){
        nextId="";
        for (var i = 0; i < 20; i++) {
            nextId += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return nextId;
        $http.post('http://localhost:8080/medicalRecords/checkIfIdExists/'+nextId).then(function(response){
            return nextId;

        },function(response){
            nextId="";
            self.getUniqueId();
        });

    }
});
patients.service("UniquePhyExIdService", function($http){

    var self = this;
    var nextId = "";
    var possible = "ABCDEFGHIkLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    self.getUniqueId = function(){
        nextId="";
        for (var i = 0; i < 20; i++) {
            nextId += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return nextId;
        $http.post('http://localhost:8080/physicalExams/checkIfIdExists/'+nextId).then(function(response){
            return nextId;

        },function(response){
            nextId="";
            self.getUniqueId();
        });

    }
});
patients.service("UniqueVitalSignIdService", function($http){

    var self = this;
    var nextId = "";
    var possible = "ABCDEFGHIkLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    self.getUniqueId = function(){
        nextId="";
        for (var i = 0; i < 20; i++) {
            nextId += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return nextId;
        $http.post('http://localhost:8080/vitalSigns/checkIfIdExists/'+nextId).then(function(response){
            return nextId;

        },function(response){
            nextId="";
            self.getUniqueId();
        });

    }
});
patients.service("UniqueSysRevIdService", function($http){

    var self = this;
    var nextId = "";
    var possible = "ABCDEFGHIkLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    self.getUniqueId = function(){
        nextId="";
        for (var i = 0; i < 20; i++) {
            nextId += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return nextId;
        $http.post('http://localhost:8080/systemReviews/checkIfIdExists/'+nextId).then(function(response){
            return nextId;

        },function(response){
            nextId="";
            self.getUniqueId();
        });

    }
});
patients.service("UniqueOrderWrapperIdService", function($http){

    var self = this;
    var nextId = "";
    var possible = "ABCDEFGHIkLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    self.getUniqueId = function(){
        nextId="";
        for (var i = 0; i < 20; i++) {
            nextId += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return nextId;
        $http.post('http://localhost:8080/orderWrappers/checkIfIdExists/'+nextId).then(function(response){
            return nextId;

        },function(response){
            nextId="";
            self.getUniqueId();
        });

    }
});

patients.service("UniqueMedChartIdService", function($http){

    var self = this;
    var nextId = "";
    var possible = "ABCDEFGHIkLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    self.getUniqueId = function(){
        nextId="";
        for (var i = 0; i < 20; i++) {
            nextId += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return nextId;
        $http.post('http://localhost:8080/medicalCharts/checkIfIdExists/'+nextId).then(function(response){
            return nextId;

        },function(response){
            nextId="";
            self.getUniqueId();
        });

    }
});

patients.controller('patientsController', function(LoginService, $scope, $http, $rootScope, $route, $cookies, $location, UniqueOrderWrapperIdService, UniqueVitalSignIdService, UniqueSysRevIdService, UniqueMedChartIdService, UniquePhyExIdService, UniqueIdService, dateService, $window, UniqueMedRecIdService){
    $scope.patients= [];
    $rootScope.patientsAttended = [];
    function formatDate(date){
        var todayTime;
        if(date){
            todayTime = new Date(date);
        }else{
            todayTime = new Date();
        }
        var day = todayTime.getDate();
        var month = todayTime.getMonth()+1;
        var year = todayTime.getFullYear();
        var hours = todayTime.getHours();
        var minutes = todayTime.getMinutes();
        var seconds = todayTime.getSeconds();

        if(day<10){
            day = "0"+day;
        }
        if(month<10){
            month =  "0"+month;
        }
        if(hours<10){
            hours =  "0"+hours;
        }
        if(minutes<10){
            minutes =  "0"+minutes;
        }
        if(seconds<10){
            seconds =  "0"+seconds;
        }

        return year+"-"+month+"-" +day+"T"+hours+":"+minutes+":"+seconds+"+08:00";
    }

    $http.get('http://localhost:8080/patients/findAllPatients').then(function(response){
        $scope.patients = response.data;
    });

    $rootScope.patientsHandled = [];
    $http.get('http://localhost:8080/doctors/patientsHandled/'+$cookies.get('username')+"/"+$cookies.get('passWord')).then(function(response){
        $rootScope.patientsHandled = response.data;
    });
    /*CURRENT TASK*/
    /*$http.get('http://localhost:8080//medicalCharts/findCurrentMedChartByPatientId/'+patientId).then(function(response){
     $rootScope.currentMedicalChart =response.data;
     console.log($rootScope.currentMedicalChart);
     });*/
    $http.get('http://localhost:8080/doctors/findDoctorByUserId/' + $cookies.get('username') + "/" + $cookies.get('passWord'))
        .then(function (response) {
            $scope.doctor = response.data;
            console.log($scope.doctor);
            var doctor = $scope.doctor;
            console.log(doctor);
            $http.get('http://localhost:8080/doctors/findAllPatientsAttended/'+ $cookies.get('username') + "/" + $cookies.get('passWord')).then(function(response){
                $rootScope.patientsAttended = response.data;
                console.log($rootScope.patientsAttended);
            });
        });

    $scope.handle = function(patientToHandle){
        $rootScope.handlePatient = patientToHandle;
        $location.path('HandlePatient');
    };
    $scope.verifyIdMessage = {};
    $scope.handleVerify = function(idNumber){
        var patient = $rootScope.handlePatient;
        var ids = [];
        $http({
            url: 'http://localhost:8080/ids/checkIfIdExists/'+idNumber,
            dataType: 'json',
            method: 'POST',
            data: $rootScope.handlePatient,
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function(response){
            $http({
                url: 'http://localhost:8080/doctors/addToHandledPatients/'+$cookies.get('username')+"/"+$cookies.get('passWord'),
                dataType: 'json',
                method: 'POST',
                data: patient,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function(response){
                $scope.response = response;
                $http({
                    url: 'http://localhost:8080/patients/updatePatientForHandle/'+$cookies.get('username')+"/"+$cookies.get('passWord'),
                    dataType: 'json',
                    method: 'POST',
                    data: patient,
                    headers: {
                        "Content-Type": "application/json"
                    }
                });
                dateService.getDate().then(function(data) {
                    $scope.dateString = data.formatted;
                    $http.get('http://localhost:8080/doctors/findDoctorByUserId/' + $cookies.get('username') + "/" + $cookies.get('passWord'))
                        .then(function (response) {
                        $rootScope.doctor = response.data;
                        var dateCreated = formatDate($scope.dateString);
                        var medicalChartToBeAdded = {
                            "AdmittingDoctor": $rootScope.doctor.firstName + " " + $rootScope.doctor.middleName + " " + $rootScope.doctor.lastName + " M.D",
                            "medicalChartId": UniqueMedChartIdService.getUniqueId(),
                            "medicalRecordId": $rootScope.handlePatient.medicalRecordID,
                            "patientId": $rootScope.handlePatient.patientId,
                            "isCurrent": true,
                            "date": dateCreated,
                            "historyPolyuria": $scope.MedChartPolyuria,
                            "historyPolydipsia": $scope.MedChartPolydispsia,
                            "historyBlurredVision": $scope.MedChartBlurredVision,
                            "historyDiaphoresis": $scope.MedChartDiaphoresis,
                            "historyAgitation": $scope.MedChartAgitation,
                            "historyTremor": $scope.MedChartTremor,
                            "historyPalpitations": $scope.MedChartPalpitation,
                            "historyInsomia": $scope.MedChartInsomnia,
                            "historyConfusion": $scope.MedChartConfusion,
                            "historyLethargy": $scope.MedChartLethargy,
                            "historySomnolence": $scope.MedChartSomnolence,
                            "historyAmnesia": $scope.MedChartAmnesia,
                            "historyStupor": $scope.MedChartStupor,
                            "historySeizures": $scope.MedChartSeizures,
                            "treatment": $scope.medicalTreatment,
                            "counsellingTime": $scope.counsellingTime,
                            "coordinationOfCaretime": $scope.coordinationOfCaretime,
                            "returnVisit": $scope.returnVisit,
                            "disposition": $scope.disposition,
                            "reasonForAdmission": $scope.reasonForAdmission,
                            "primaryDiagnosis": $scope.primaryDiagnosis,
                            "differentialDiagnosis": $scope.differentialDiagnosis,
                            "current": true
                        };
                        $http({
                            url: 'http://localhost:8080/medicalCharts/create',
                            dataType: 'json',
                            method: 'POST',
                            data: medicalChartToBeAdded,
                            headers: {
                                "Content-Type": "application/json"
                            }
                        }).then(function(response){
                            swal("Success!", "Patient has been successfully handled.", "success");
                            $route.reload();
                        },function(error){
                            swal("Failed", error.data, "error");
                        });
                });
            });

            },function(response){
                swal("Failed", "Patient already handled", "error");
            }).catch(function onError(error) {
                console.log(error);
            });
        },function(response){
            swal("Failed", "The I.D. number entered is incorrect. Please try again.", "error");
        });
    }
    $scope.addPatient = function(){
        dateService.getDate().then(function(data){
            $scope.dateString = data.formatted;

            var dateToday = new Date($scope.dateString);
            var birthDate = new Date($scope.birthDate);//2017-10-01

            var bdayYear = birthDate.getFullYear();
            var bdayMonth = birthDate.getMonth()+1;
            var bdayDate = birthDate.getDate();

            var todayYear = dateToday.getFullYear();
            var todayMonth = dateToday.getMonth()+1;
            var todayDate = dateToday.getDate();

            if(bdayMonth>todayMonth){
                var age = (todayYear - bdayYear)-1;
            }
            else if(bdayMonth<todayMonth){
                var age = todayYear - bdayYear;
            }
            else{
                if(bdayDate>todayDate){
                    var age = (todayYear - bdayYear)-1;
                }
                else{
                    var age = todayYear - bdayYear;
                }
            }
            function toTitleCase(str) {
                return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
            }

            function formatDate(date){
                var todayTime;
                if(date){
                    todayTime = new Date(date);
                }else{
                    todayTime = new Date();
                }
                var day = todayTime.getDate();
                var month = todayTime.getMonth()+1;
                var year = todayTime.getFullYear();

                if(day<10){
                    day = "0"+day;
                }
                if(month<10){
                    month =  "0"+month;
                }
                return year+"-"+month+"-" +day;


            }

            var fn1 = $scope.firstName;
            var fn = fn1.toLowerCase();
            var firstName = toTitleCase(fn);

            var mn1 = $scope.middleName;
            var mn = mn1.toLowerCase();
            var middleName = toTitleCase(mn);

            var ln1 = $scope.lastName;
            var ln = ln1.toLowerCase();
            var lastName = toTitleCase(ln)

            var st1 = $scope.street;
            var st = st1.toLowerCase();
            var street = toTitleCase(st);

            var br1 = $scope.baranggay;
            var br = br1.toLowerCase();
            var brgy = br.charAt(0).toUpperCase()+br.substring(1);

            var ct1 = $scope.city;
            var ct = ct1.toLowerCase();
            var city = toTitleCase(ct);

            var rel1 = $scope.religion;
            var rel = rel1.toLowerCase();
            var religion = rel.charAt(0).toUpperCase()+rel.substring(1);

            var na1 = $scope.nationality;
            var na = na1.toLowerCase();
            var nationality = na.charAt(0).toUpperCase()+na.substring(1);

            var lang1 = $scope.language;
            var lang = lang1.toLowerCase();
            var language = lang.charAt(0).toUpperCase()+lang.substring(1);

            var bday = formatDate($scope.birthDate);


            var patientToBeAdded = {
                "patientId": UniqueIdService.getUniqueId(),
                "firstName": firstName,
                "gender": $scope.gender,
                "middleName": middleName,
                "lastName": lastName,
                "houseNumber": "#"+$scope.houseNumber,
                "street": street + " st.",
                "baranggay": "Brgy. " + brgy,
                "city": city,
                "zipCode": $scope.zipCode,
                "dateOfBirth": bday,
                "dependencyList": null,
                "ids": null,
                "age": age,
                "religion": religion,
                "nationality": nationality,
                "language": language,
                "civilStatus": $scope.civilStatus,
                "contactNumber": $scope.contactNumber,
                "email": $scope.email,
                "currentHospital_Id": null,
                "medicalRecordID": UniqueMedRecIdService.getUniqueId(),
                "handled": false
            }

            var medicalRecordToBeAdded = {
                "medicalRecordId": patientToBeAdded.medicalRecordID,
                "patientId": patientToBeAdded.patientId,
                "pastMedicalHistoryAsthma": $scope.pastAsthma,
                "pastMedicalHistoryHypertension": $scope.pastHypertension,
                "pastMedicalHistoryCva": $scope.pastCVA,
                "pastMedicalHistoryDiabetes": $scope.pastDiabetes,
                "pastMedicalHistoryCancer": $scope.pastCancer,
                "pastMedicalHistoryHeartDisease": $scope.pastHeartDisease,
                "pastMedicalHistoryLungDisease": $scope.pastLungDisease,
                "pastMedicalHistoryLiverDisease": $scope.pastLiverDisease,
                "pastMedicalHistoryKidneyDisease": $scope.pastKidneyDisease,
                "familyAsthma": $scope.familyAsthma,
                "familyHypertension": $scope.familyHypertension,
                "familyCva": $scope.familyCVA,
                "familyDiabetes": $scope.familyDiabetes,
                "familyCancer": $scope.familyCancer,
                "familyHeartDisease": $scope.familyHeartDisease,
                "familyLungDisease": $scope.familyLungDisease,
                "familyLiverDisease": $scope.familyLiverDisease,
                "familyKidneyDisease": $scope.familyKidneyDisease,
                "immunizationBacilleCalmetteGuerin": $scope.immuBacilleCalmetteGuerin,
                "immunizationChickenpoxVaccine": $scope.immuChickenpoxVaccine,
                "immunizationDiphtheria": $scope.immuDiphtheria,
                "immunizationHepatitisAVaccine": $scope.immuHepatitisAVaccine,
                "immunizationHepatitisBVaccine": $scope.immuHepatitisBVaccine,
                "immunizationHibVaccine": $scope.immuHibVaccine,
                "immunizationHumanPappillomavirus": $scope.immuHumanPapillomaVirus,
                "immunizationInfluenzaVaccine": $scope.immuInfluenzaVaccine,
                "immunizationMeasles": $scope.immuMeasles,
                "immunizationTetanus": $scope.immuTetanus,
                "immunizationPertussisVaccine": $scope.immuPertussisVaccine,
                "immunizationMumps": $scope.immuMumps,
                "immunizationRubellaVaccine": $scope.immuRubellaVaccine,
                "immunizationMeningococcalVaccine": $scope.immuMengincoccalVaccine,
                "immunizationPneumococcalVaccine": $scope.immuPneumococcalVaccine,
                "immunizationPolioVaccine": $scope.immuPolioVaccine,
                "immunizationRotarusVaccine": $scope.immuRotarusVaccine
                /*"otherVaccine": */

            };
            var SSS = $scope.SSSnumber;
            var TIN = $scope.TINnumber;
            var Driver = $scope.DriversLicenseNumber;

            if(SSS != null){
                var IdToBeAdded = {
                    "idType": "SSS",
                    "idNumber": SSS,
                    "patientId":patientToBeAdded.patientId
                };
                $http({
                    url: 'http://localhost:8080//ids/createId',
                    dataType: 'json',
                    method: 'POST',
                    data: IdToBeAdded,
                    headers: {
                        "Content-Type": "application/json"
                    }
                });
            }
            if(TIN != null){
                var IdToBeAdded = {
                    "idType": "TIN",
                    "idNumber": TIN,
                    "patientId": patientToBeAdded.patientId
                };
                $http({
                    url: 'http://localhost:8080//ids/createId',
                    dataType: 'json',
                    method: 'POST',
                    data: IdToBeAdded,
                    headers: {
                        "Content-Type": "application/json"
                    }
                });
            }
            if(Driver != null){
                var IdToBeAdded = {
                    "idType": "Driver's License",
                    "idNumber": Driver,
                    "patientId": patientToBeAdded.patientId
                };
                $http({
                    url: 'http://localhost:8080//ids/createId',
                    dataType: 'json',
                    method: 'POST',
                    data: IdToBeAdded,
                    headers: {
                        "Content-Type": "application/json"
                    }
                });
            }
            $http({
                url: 'http://localhost:8080/patients/createPatient',
                dataType: 'json',
                method: 'POST',
                data: patientToBeAdded,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function(response){
                $http({
                    url: 'http://localhost:8080/medicalRecords/create',
                    dataType: 'json',
                    method: 'POST',
                    data: medicalRecordToBeAdded,
                    headers: {
                        "Content-Type": "application/json"
                    }
                }).then(function(response){
                    swal("Success!", "Patient successfully added.", "success");
                    $route.reload();
                },function(error){
                    swal("Failed", error, "error");
                });

            });
        });
    }



    $scope.viewProfile = function(patient){
        $rootScope.viewPatient = patient;
    }
    $scope.update = function(patient){
        $http({
            url: 'http://localhost:8080/patients/updatePatient',
            dataType: 'json',
            method: 'POST',
            data: patient,
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function(response){
            $http({
                url: 'http://localhost:8080/doctors/updateHandledPatients/'+$cookies.get('username')+"/"+$cookies.get('passWord'),
                dataType: 'json',
                method: 'POST',
                data: patient,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function(response){
                swal("Success!", "Patient successfully updated.", "success");
                $route.reload();
            },function(error){
                swal("Failed", error, "error");
            })
        },function(error){
            swal("Failed", error, "error");
        });
    }

    $scope.viewMedicalRecord = function(patientId){
        $http.get('http://localhost:8080//medicalRecords/findMedRecByPatientId/'+patientId).then(function(response){
            $rootScope.patientMedRecord = response.data;
        });
    }

    $scope.viewCurrentMedicalChart = function(patientId){
        $http.get('http://localhost:8080//physicalExams/findAllPhysicalExamByPatientId/'+patientId).then(function(response){
            $rootScope.patientPhysicalExams = response.data;
        });
        $http.get('http://localhost:8080//systemReviews/findAllSystemReviewByPatientId/'+patientId).then(function(response){
            $rootScope.patientSystemReviews = response.data;
        });
        $http.get('http://localhost:8080//vitalSigns/findAllvitalSignsByPatientId/'+patientId).then(function(response){
            $rootScope.patientVitalSigns =response.data;

        });
        $http.get('http://localhost:8080//medicalCharts/findCurrentMedChartByPatientId/'+patientId).then(function(response){
            $rootScope.currentMedicalChart =response.data;
        });
        $http.get('http://localhost:8080//orderWrappers/findAllCurrentOrderWrapper/'+patientId).then(function(response){
            $rootScope.orderWrappers =response.data;
        });
    }

    $scope.redirect = function(){ //ETO YUNG NEW TAB FOR VIEWING NG PAST MEDICAL CHART
        $window.open('http://localhost:8080/test', '_blank');
        var viewPatientId = $rootScope.viewPatient.patientId;
        $cookies.put('viewPatient',viewPatientId);
    };

    $scope.addPhysicalExamModal = function(){
        dateService.getDate().then(function(data){
            $scope.dateNowString = Date.parse(data.formatted);
        });
    };

    $scope.addPhysicalExam = function(){
        dateService.getDate().then(function(data){
            $scope.dateNowString = Date.parse(data.formatted);
            $http.get('http://localhost:8080/medicalCharts/findCurrentMedChartByPatientId/'+$rootScope.viewPatient.patientId).then(function(response){
                $scope.currentMedChart = response.data;
                $http.get('http://localhost:8080/doctors/findDoctorByUserId/'+$cookies.get('username')+"/"+$cookies.get('passWord')).then(function(response){
                    $rootScope.doctor = response.data;
                    var dateNow = formatDate($scope.dateNowString)
                    var physicalExamToBeAdded = {
                        "medicalChartId": $scope.currentMedChart.medicalChartId,
                        "physicalExamId": UniquePhyExIdService.getUniqueId(),
                        "patientId": $rootScope.viewPatient.patientId,
                        "date": dateNow,
                        "conductorID": $rootScope.doctor.doctorId,
                        "contributor": $rootScope.doctor.firstName + " " +$rootScope.doctor.middleName + " " + $rootScope.doctor.lastName + " M.D",
                        "generalApperance": $scope.generalPhyEx,
                        "eyes": $scope.eyesPhyEx,
                        "nose": $scope.nosePhyEx,
                        "ears": $scope.earsPhyEx,
                        "mouth": $scope.mouthPhyEx,
                        "throat": $scope.throatPhyEx,
                        "respiratory": $scope.respiratoryPhyEx,
                        "cardioVascular": $scope.cardioPhyEx,
                        "skin": $scope.skinPhyEx,
                        "problem": $scope.problemPhyEx,
                        "impression": $scope.impressionPhyEx,
                        "notes": $scope.notesPhyEx,
                        "current": true
                    };
                    $http({
                        url: 'http://localhost:8080/physicalExams/create',
                        dataType: 'json',
                        method: 'POST',
                        data: physicalExamToBeAdded,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then(function(response){
                        swal("Success!", "Physical Exam successfully added.", "success");
                        $route.reload();
                    },function(error){
                        swal("Failed", error.message, "error");
                    });
                });
            });


        });
    };

    $scope.addSystemReviewModal = function(){
        dateService.getDate().then(function(data){
            $scope.dateNowString = Date.parse(data.formatted);
        });
    };

    $scope.addSystemReview = function(){
        dateService.getDate().then(function(data){
            $scope.dateNowString = Date.parse(data.formatted);
            $http.get('http://localhost:8080/medicalCharts/findCurrentMedChartByPatientId/'+$rootScope.viewPatient.patientId).then(function(response){
                $scope.currentMedChart = response.data;
                $http.get('http://localhost:8080/doctors/findDoctorByUserId/'+$cookies.get('username')+"/"+$cookies.get('passWord')).then(function(response){
                    $rootScope.doctor = response.data;
                    var systemReviewToBeAdded = {
                            "medicalChartId":$scope.currentMedChart.medicalChartId,
                            "patientId": $rootScope.viewPatient.patientId,
                            "systemReviewId": UniqueSysRevIdService.getUniqueId(),
                            "date": formatDate($scope.dateNowString),
                            "conductorId": $rootScope.doctor.doctorId,
                            "contributor": $rootScope.doctor.firstName + " " +$rootScope.doctor.middleName + " " + $rootScope.doctor.lastName + " M.D",
                            "general": $scope.sysRevGeneral,
                            "eyes": $scope.sysRevEyes,
                            "nose": $scope.sysRevNose,
                            "ears": $scope.sysRevEars,
                            "throat": $scope.sysRevThroat,
                            "cardioVascular": $scope.sysRevCardio,
                            "gastrointestinal": $scope.sysRevGastro,
                            "genitourinary": $scope.sysRevGenito,
                            "musculoskeletal": $scope.sysRevMusculo,
                            "skin": $scope.sysRevSkin,
                            "neurologic": $scope.sysRevNeurologic,
                            "psychiatric": $scope.sysRevPsychiatric,
                            "endocrine": $scope.sysRevEndocrine,
                            "hemelympathic": $scope.sysRevHemelympathic,
                            "allergicimmunologic": $scope.sysRevAllergicimmunologic,
                            "notes": $scope.sysRevNotes,
                            "current": true
                    };
                    $http({
                        url: 'http://localhost:8080/systemReviews/create',
                        dataType: 'json',
                        method: 'POST',
                        data: systemReviewToBeAdded,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then(function(response){
                        swal("Success!", "Physical Exam successfully added.", "success");
                        $route.reload();
                    },function(error){
                        swal("Failed", error.message, "error");
                    });
                });
            });


        });
    };

    $scope.addVitalSignModal = function(){
        dateService.getDate().then(function(data){
            $scope.dateNowString = Date.parse(data.formatted);
        });
    };

    $scope.addVitalSign = function(){
        dateService.getDate().then(function(data){
            $scope.dateNowString = Date.parse(data.formatted);
            $http.get('http://localhost:8080/medicalCharts/findCurrentMedChartByPatientId/'+$rootScope.viewPatient.patientId).then(function(response){
                $scope.currentMedChart = response.data;
                $http.get('http://localhost:8080/doctors/findDoctorByUserId/'+$cookies.get('username')+"/"+$cookies.get('passWord')).then(function(response){
                    $rootScope.doctor = response.data;
                    var vitalSignToBeAdded = {
                        "vitalSignsId": UniqueVitalSignIdService.getUniqueId(),
                        "medicalChartId": $scope.currentMedChart.medicalChartId,
                        "patientId": $rootScope.viewPatient.patientId,
                        "collectionDate": formatDate($scope.dateNowString),
                        "contributorId": $rootScope.doctor.doctorId,
                        "contributor": $rootScope.doctor.firstName + " " +$rootScope.doctor.middleName + " " + $rootScope.doctor.lastName + " M.D",
                        "heightCm": parseInt($scope.vitSignHeightCm),
                        "weightKg": parseInt($scope.vitSignWeightKg),
                        "temperatureCelcius": $scope.vitSignTemperatureCelcius,
                        "temperatureSite": $scope.vitSignTemperatureSite,
                        "pulsePerMinute": $scope.vitSignPulsePerMinute,
                        "pulseType": $scope.vitSignPulseType,
                        "respirationPerMinute": $scope.vitSignRespirationPerMinute,
                        "bloodPressure": $scope.vitSignBloodPressure,
                        "notes": $scope.vitSignNotes,
                        "current": true
                    };
                    $http({
                        url: 'http://localhost:8080/vitalSigns/create',
                        dataType: 'json',
                        method: 'POST',
                        data: vitalSignToBeAdded,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then(function(response){
                        swal("Success!", "Physical Exam successfully added.", "success");
                        $route.reload();
                    },function(error){
                        swal("Failed", error.message, "error");
                    });
                });
            });


        });
    };

    $scope.AddOrderWrapper = function(){
        dateService.getDate().then(function(data){
            $scope.dateNowString = Date.parse(data.formatted);
            $http.get('http://localhost:8080/medicalCharts/findCurrentMedChartByPatientId/'+$rootScope.viewPatient.patientId).then(function(response){
                $scope.currentMedChart = response.data;
                var orderWrapperToBeAdded = {
                    "medicalChartId": $scope.currentMedChart.medicalChartId,
                    "orderWrapperId": UniqueOrderWrapperIdService.getUniqueId(),
                    "patientId": $rootScope.viewPatient.patientId,
                    "creationDate": formatDate($scope.dateNowString),
                    "doctorId": $rootScope.doctor.doctorId,
                    "isCurrent": true,
                    "createdBy": $rootScope.doctor.firstName + " " +$rootScope.doctor.middleName + " " + $rootScope.doctor.lastName + " M.D",
                    "specialization": $rootScope.doctor.expertise
                };
                $http({
                    url: 'http://localhost:8080/orderWrappers/create',
                    dataType: 'json',
                    method: 'POST',
                    data: orderWrapperToBeAdded,
                    headers: {
                        "Content-Type": "application/json"
                    }
                }).then(function(response){
                    swal("Success!", "Doctor's Order Record successfully created.", "success");
                    $route.reload();
                },function(error){
                    swal("Failed", "error", "error");
                });
            });

        });
    };
    $scope.viewOrder = function(orderWrapperId){
        $window.open('http://localhost:8080/viewDoctorsOrder', '_blank');
        var viewPatientId = $rootScope.viewPatient.patientId;
        $cookies.put('viewPatient',viewPatientId);
        $cookies.put('wrapperId',orderWrapperId);
    };

    $scope.addAttendingDoctor = function(){
      $http.get('http://localhost:8080/doctors/findAllDoctors').then(function(response){
         $scope.listOfDoctors = response.data;
      });
    };

    $scope.addToListOfAttendingDoctors = function(doctor){
        $http.get('http://localhost:8080/medicalCharts/findCurrentMedChartByPatientId/'+$rootScope.viewPatient.patientId).then(function(response){
            $scope.currentMedChart = response.data;
            $http({
                url: 'http://localhost:8080/medicalCharts/addAttendingDoctor/'+$scope.currentMedChart.medicalChartId,
                dataType: 'json',
                method: 'POST',
                data: doctor,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function(response){
                $http.get('http://localhost:8080/doctors/findDoctorByUserId/' + $cookies.get('username') + "/" + $cookies.get('passWord'))
                    .then(function (response) {
                        $rootScope.doctor = response.data;
                        console.log($rootScope.doctor);
                        $http.get('http://localhost:8080/patients/findPatientById/' + $rootScope.viewPatient.patientId)
                            .then(function (response) {
                                var patient = response.data;
                                $http({
                                    url: 'http://localhost:8080/doctors/addToPatientsAttended/' + doctor.id,
                                    dataType: 'json',
                                    method: 'POST',
                                    data: patient,
                                    headers: {
                                        "Content-Type": "application/json"
                                    }
                                }).then(function (response) {
                                    swal("Success!", "Doctor has been added.", "success");
                                    $route.reload();
                                });

                            });
                    });
            });

        });

    }
    $scope.viewAttendingDoctors = function(){
        $http.get('http://localhost:8080/medicalCharts/findCurrentMedChartByPatientId/'+$rootScope.viewPatient.patientId).then(function(response) {
            $scope.currentMedChart = response.data;
            $scope.attendingDoctors = [];
            $scope.attendingDoctors = $scope.currentMedChart.attendingDoctorList;
        });
    };

    $scope.viewPastMedCharts = function(){
        $http.get('http://localhost:8080/medicalCharts/findPastMedChartByPatientId/'+$rootScope.viewPatient.patientId).then(function(response){
            $scope.pastcharts = response.data;
        });
    };




});


