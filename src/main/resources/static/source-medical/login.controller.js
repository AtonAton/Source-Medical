'use strict'
var login = angular.module('sourceMedApp');

login.controller('loginController', function(LoginService, $scope, $location, $rootScope, $route, $cookies){
    $scope.submit = function(){
        var uId = $scope.userId;
        var pword = $scope.passWord;

        setLoginCredentials(uId,pword);
        getLoggedInUser(uId,pword);

        function getLoggedInUser(uId,pword) {
            LoginService.getLoggedInUser(uId,pword)
                .then(function (loggedInUser) {
                    $rootScope.loggedDoctor = loggedInUser;
                    if($rootScope.loggedDoctor != null) {
                        $location.path('/dashboard');
                        window.location.reload();
                    }
                },
                function (errorMessage) {
                    console.log(errorMessage);
                });
        }
        function setLoginCredentials(uId, pword){
            $cookies.put('username',uId);
            $cookies.put('passWord',pword);
        }

    }

});
