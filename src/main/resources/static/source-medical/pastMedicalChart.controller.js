'use strict';
var pastMEdChart = angular.module('sourceMedApp');

pastMEdChart.service("UniqueOrderIdService", function($http){

    var self = this;
    var nextId = "";
    var possible = "ABCDEFGHIkLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    self.getUniqueId = function(){
        nextId="";
        for (var i = 0; i < 20; i++) {
            nextId += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return nextId;
        $http.post('http://localhost:8080/physicalExams/checkIfIdExists/'+nextId).then(function(response){
            return nextId;

        },function(response){
            nextId="";
            self.getUniqueId();
        });

    }
});

pastMEdChart.controller('pastMEdChartController', function(LoginService, $scope, $http, $rootScope, $route, $cookies, $location, UniqueOrderIdService, UniqueOrderWrapperIdService, UniqueVitalSignIdService, UniqueSysRevIdService, UniqueMedChartIdService, UniquePhyExIdService, UniqueIdService, dateService, $window, UniqueMedRecIdService){
    var patientId = $cookies.get('viewPatient');
    var orderWrapperId = $cookies.get('wrapperId');

    $http.get('http://localhost:8080/doctors/findDoctorByUserId/'+$cookies.get('username')+"/"+$cookies.get('passWord')).then(function(response){
        $rootScope.doctor = response.data;});

    function formatDate(date){
        var todayTime;
        if(date){
            todayTime = new Date(date);
        }else{
            todayTime = new Date();
        }
        var day = todayTime.getDate();
        var month = todayTime.getMonth()+1;
        var year = todayTime.getFullYear();
        var hours = todayTime.getHours();
        var minutes = todayTime.getMinutes();
        var seconds = todayTime.getSeconds();

        if(day<10){
            day = "0"+day;
        }
        if(month<10){
            month =  "0"+month;
        }
        if(hours<10){
            hours =  "0"+hours;
        }
        if(minutes<10){
            minutes =  "0"+minutes;
        }
        if(seconds<10){
            seconds =  "0"+seconds;
        }

        return year+"-"+month+"-" +day+"T"+hours+":"+minutes+":"+seconds+"+08:00";
    }

    $http.get('http://localhost:8080//physicalExams/findAllPhysicalExamByPatientId/'+patientId).then(function(response){
        $rootScope.patientPhysicalExams = response.data;
    });
    $http.get('http://localhost:8080//systemReviews/findAllSystemReviewByPatientId/'+patientId).then(function(response){
        $rootScope.patientSystemReviews = response.data;
    });
    $http.get('http://localhost:8080//vitalSigns/findAllvitalSignsByPatientId/'+patientId).then(function(response){
        $rootScope.patientVitalSigns =response.data;
    });
    $http.get('http://localhost:8080/patients/findPatientById/'+patientId).then(function(response){
        $rootScope.viewPatient = response.data;
    });

    $http.get('http://localhost:8080/orderWrappers/findByOrderWrapper/'+orderWrapperId).then(function(response){
        $rootScope.orderWrapper = response.data;
    });

    $http.get('http://localhost:8080/doctorsOrder/findAllOrdersByWrapperId/'+orderWrapperId).then(function(response){
       $rootScope.orders = response.data;
    });

    $scope.addOrder = function(){
        dateService.getDate().then(function(data){
            $scope.dateString = data.formatted;
            $http.get('http://localhost:8080/medicalCharts/findCurrentMedChartByPatientId/'+$rootScope.viewPatient.patientId).then(function(response) {
                $scope.currentMedChart = response.data;
                $http.get('http://localhost:8080/doctors/findDoctorByUserId/'+$cookies.get('username')+"/"+$cookies.get('passWord')).then(function(response) {
                    $rootScope.doctor = response.data;
                    var orderToBeAdded = {
                        "medicalChartId": $scope.currentMedChart.medicalChartId,
                        "orderId": UniqueOrderIdService.getUniqueId(),
                        "orderWrapperId": orderWrapperId,
                        "patientId": patientId,
                        "creationDate": formatDate($scope.dateString),
                        "order": $scope.order,
                        "doctorId": $rootScope.doctor.doctorId,
                        "contributor": $rootScope.doctor.firstName + " " +$rootScope.doctor.middleName + " " + $rootScope.doctor.lastName + " M.D",
                        "isCurrent": true,
                        "isCompleted": false,
                        "status": "Pending"
                    };
                    $http({
                        url: 'http://localhost:8080/doctorsOrder/createDoctorsOrder',
                        dataType: 'json',
                        method: 'POST',
                        data: orderToBeAdded,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then(function(response){
                        swal({
                            title: "Success!",
                            text: "Order has been added.",
                            type: "success"
                        },function(){
                            $window.location.reload();
                        });
                    },function(error){
                        swal("Failed", "error", "error");
                    });
                });
            });
        });
    };
    $scope.removeOrder = function(orderId){
            swal({    title: "Are you sure?",
                    text: "You will not be able to recover this order!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function(){
                    $http({
                        url: 'http://localhost:8080/doctorsOrder/delete/'+orderId,
                        dataType: 'json',
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then(function(response){
                        swal({
                            title: "Success!",
                            text: "Order has been removed.",
                            type: "success"
                    },function(){
                        $window.location.reload();
                    });
                },
                function(){
                    return;
                });
        });
    }
    $scope.handleOrder = function(order) {
        $scope.handleTitle = order.order;
        $rootScope.orderId = order.orderId;
    }

    $scope.submitHandle = function(){
        var procedure = $scope.procedure;
        var laboratory = $scope.laboratory;
        var medication = $scope.medication;
        $http.get('http://localhost:8080/medicalCharts/findCurrentMedChartByPatientId/'+$rootScope.viewPatient.patientId).then(function(response) {
            $scope.currentMedChart = response.data;
            var medChart = $scope.currentMedChart;

            if(procedure!=null){
                if(medChart.operations !=null){
                    medChart.operations.push(procedure);
                }
                else{
                    medChart.operations = new Array();
                    medChart.operations.push(procedure);
                }
            }
            if(laboratory!=null){
                if(medChart.labExams !=null){
                    medChart.labExams.push(laboratory);
                }
                else{
                    medChart.labExams = new Array();
                    medChart.labExams.push(laboratory);
                }
            }
            if(medication!=null){
                if(medChart.medications !=null){
                    medChart.medications.push(medication);
                }
                else{
                    medChart.medications = new Array();
                    medChart.medications.push(medication);
                }
            }
            medChart.current = true;
            console.log($scope.currentMedChart);
            console.log($rootScope.doctor);
            $http({
                url: 'http://localhost:8080/medicalCharts/update/'+$scope.currentMedChart.medicalChartId,
                dataType: 'json',
                method: 'POST',
                data: medChart,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function(response){
                $http.get('http://localhost:8080/doctorsOrder/findByOrderId/'+$rootScope.orderId).then(function(response){
                    $scope.orderFromDb = response.data;
                    console.log($rootScope.orderId);
                    var orderTobeUpdated = {

                        "medicalChartId": $scope.orderFromDb.medicalChartId,
                        "orderId": $scope.orderFromDb.orderId,
                        "orderWrapperId": $scope.orderFromDb.orderWrapperId,
                        "patientId": $scope.orderFromDb.patientId,
                        "creationDate": $scope.orderFromDb.creationDate,
                        "order": $scope.orderFromDb.order,
                        "doctorId": $scope.orderFromDb.doctorId,
                        "contributor": $scope.orderFromDb.contributor,
                        "isCurrent": true,
                        "isCompleted": true,
                        "completedBy": $rootScope.doctor.firstName + " " + $rootScope.doctor.middleName + " " + $rootScope.doctor.lastName + " M.D",
                        "status": "Completed"
                    }
                    console.log(orderTobeUpdated);
                    $http({
                        url: 'http://localhost:8080/doctorsOrder/update/'+$rootScope.orderId,
                        dataType: 'json',
                        method: 'POST',
                        data: orderTobeUpdated,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    }).then(function(response){
                        console.log(medChart);
                        swal({
                            title: "Success!",
                            text: "Order has been completed.",
                            type: "success"
                        },function(){

                            $window.location.reload();
                            console.log("HOY");
                            console.log(medChart);
                        });
                    })


                });
            },function(error){
                swal("Failed", "error", "error");
            });

        });
    };

});