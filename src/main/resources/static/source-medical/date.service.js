'use strict'
angular.module('sourceMedApp').factory('dateService', ['$http', '$q',
    function ($http, $q) {

        var factory = {
            getDate: getDate,
        };

        return factory;

        function getDate() {
            var deferred = $q.defer();
            $http.get('http://api.timezonedb.com/v2/get-time-zone?key=IMQ6LT5SC3I9&format=json&by=zone&zone=Asia/Hong_Kong')
                .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }
    }
]);