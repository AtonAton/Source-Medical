'use strict'
angular.module('loginService').factory('LoginService', ['$http', '$q',
    function ($http, $q) {

        var factory = {
            getLoggedInUser: getLoggedInUser,
        };

        return factory;

        function getLoggedInUser(uId,pass) {
            var deferred = $q.defer();
            $http.get('/doctors/findDoctorByUserId/'+uId+'/'+pass)
                .then(
                function (loggedInUser) {
                    deferred.resolve(loggedInUser.data);
                },
                function (errResponse) {
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }
    }
]);


