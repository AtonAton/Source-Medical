'use strict'
var sourceMed = angular.module('sourceMedApp', ['ngRoute', 'angularUtils.directives.dirPagination', 'loginService', 'ngCookies']);

sourceMed.config(['$qProvider','$routeProvider','$locationProvider', function($qProvider,$routeProvider, $locationProvider){
    $routeProvider
        /*.when('/login', {
            templateUrl: 'login.html',
            controller: 'loginController'
        })*/
        .when('/SearchPatient',{
            templateUrl: 'views/SearchForPatient.html',
            controller: 'patientsController'
        })
        .when('/Profile',{
            templateUrl: 'views/ViewProfile.html',
            controller: 'dashboardController'
        })
        .when('/LeadByMe',{
            templateUrl: 'views/LeadByMe.html',
            controller: 'patientsController'
        })
        .when('/HandlePatient',{
            templateUrl: 'views/HandlePatient.html',
            controller: 'patientsController'
        })
        .when('/AttendedByMe',{
            templateUrl: 'views/AttendedByMe.html',
            ontroller: 'patientsController'
        })
        .when('/ViewPatient',{
            templateUrl: 'views/PatientHTML-2.html',
            controller: 'patientsController'
        });
        /*.otherwise({
            redirectTo: '/'
        });*/
    $locationProvider.html5Mode(true);
}]);
